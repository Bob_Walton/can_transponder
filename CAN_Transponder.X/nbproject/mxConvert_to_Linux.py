import os.path
import shutil


def Convert_Makefile_default_mk():
    with open("Makefile-default.mk_org") as f:
        with open("Makefile-default.mk", "w") as f1:
            line = f.readline()
            while line:
                if line[:1] == '#':  #if coment just pass through
                    f1.write(line)
                elif line[:6] == "MKDIR=":
                    f1.write("MKDIR=mkdir -p\n")

                elif line[:24] == "\t${MAKE} ${MAKE_OPTIONS}":
                    f1.write("\t${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}\n")
                elif line[:12] == "\t${MP_CC_DIR":
                    f1.write("\t${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} \n")
                elif line[:9] == "DEPFILES=":
                    f1.write("DEPFILES=$(shell \"${PATH_TO_IDE_BIN}\"mplabwildcard ${POSSIBLE_DEPFILES})\n")
                else:
                    f1.write(line)
                line = f.readline()




def Convert_Makefile_local_default_mk():
    with open("Makefile-local-default.mk_org") as f:
        with open("Makefile-local-default.mk", "w") as f1:
            line = f.readline()
            while line:
                if line[:1] == '#':  #if coment just pass through
                    f1.write(line)
                elif line[:6] == "SHELL=":
                    pass
                elif line[:16] == "PATH_TO_IDE_BIN=":
                    f1.write("PATH_TO_IDE_BIN=/opt/microchip/mplabx/mplab_ide/mplab_ide/modules/../../bin/\n")
                elif line[:6] == "PATH:=":
                    f1.write("PATH:=/opt/microchip/mplabx/mplab_ide/mplab_ide/modules/../../bin/:$(PATH)\n")
                elif line[:13] == "MP_JAVA_PATH=":
                    f1.write("MP_JAVA_PATH=\"/opt/microchip/mplabx/sys/java/jre1.7.0_25/bin/\"\n")
                elif line[:6] == "MP_CC=":
                    f1.write("MP_CC=\"/opt/microchip/xc32/v1.32/bin/xc32-gcc\"\n")
                elif line[:8] == "MP_CPPC=":
                    f1.write("MP_CPPC=\"/opt/microchip/xc32/v1.32/bin/xc32-g++\"\n")
                elif line[:6] == "MP_AS=":
                    f1.write("MP_AS=\"/opt/microchip/xc32/v1.32/bin/xc32-as\"\n")
                elif line[:6] == "MP_LD=":
                    f1.write("MP_LD=\"/opt/microchip/xc32/v1.32/bin/xc32-ld\"\n")
                elif line[:6] == "MP_AR=":
                    f1.write("MP_AR=\"/opt/microchip/xc32/v1.32/bin/xc32-ar\"\n")
                elif line[:8] == "DEP_GEN=":
                    f1.write("DEP_GEN=${MP_JAVA_PATH}java -jar \"/opt/microchip/mplabx/mplab_ide/mplab_ide/modules/../../bin/extractobjectdependencies.jar\" \n")
                elif line[:10] == "MP_CC_DIR=":
                    f1.write("MP_CC_DIR=\"/opt/microchip/xc32/v1.32/bin\"\n")
                elif line[:12] == "MP_CPPC_DIR=":
                    f1.write("MP_CPPC_DIR=\"/opt/microchip/xc32/v1.32/bin\"\n")
                elif line[:10] == "MP_AS_DIR=":
                    f1.write("MP_AS_DIR=\"/opt/microchip/xc32/v1.32/bin\"\n")
                elif line[:10] == "MP_LD_DIR=":
                    f1.write("MP_LD_DIR=\"/opt/microchip/xc32/v1.32/bin\"\n")
                elif line[:10] == "MP_AR_DIR=":
                    f1.write("MP_AR_DIR=\"/opt/microchip/xc32/v1.32/bin\"\n")
                else:
                    f1.write(line)
                line = f.readline()



# main function opens file passed in command line, and calls the
# necessary functions to parse it
def main():
    print "mxConvert_to_Linux"
    
    if not os.path.isfile("Makefile-local-default.mk_org") :
        print "Makefile-local-default.mk_org Not Found. Copying file"
        shutil.copy2("Makefile-local-default.mk","Makefile-local-default.mk_org")
    
    if not os.path.isfile("Makefile-default.mk_org") :
        print "Makefile-default.mk_org  Not Found. Copying file"
        shutil.copy2("Makefile-default.mk","Makefile-default.mk_org")
        
        
        
    Convert_Makefile_local_default_mk()
    Convert_Makefile_default_mk()
                    


# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    main()

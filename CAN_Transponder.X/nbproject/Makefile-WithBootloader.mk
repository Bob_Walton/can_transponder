#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-WithBootloader.mk)" "nbproject/Makefile-local-WithBootloader.mk"
include nbproject/Makefile-local-WithBootloader.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=WithBootloader
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../Application/Externals/CAN_Application/CAN_Application.c ../Application/Externals/Ethernet_Application/ETH_Application.c ../Application/Externals/Magni_Application/Magni_Application.c ../Application/Externals/Pcs_Application/Pcs_Application.c ../Application/Externals/Rf_Application/Rf_Application.c ../Application/Externals/Status_Application/Status_Application.c ../Application/Externals/UART_Application/UART_Application.c ../Drivers/Externals/4to20mA_Drivers/4to20mA_Drv_PIC32MX575L.c ../Drivers/Externals/CAN_Drivers/CAN_Drv_PIC32MX575L.c ../Drivers/Externals/Ethernet_Drivers/ETH_Drv_PIC32MX575L.c ../Drivers/Externals/Ethernet_Drivers/PBP_ENCX24J600.c ../Drivers/Externals/GPIO_Drivers/GPIO_Drv_PIC32MX575L.c ../Drivers/Externals/Magni_Drivers/Magni_Drv_PIC32MX575L.c ../Drivers/Externals/Rf_Drivers/Rf_Drv_RS9110eth.c ../Drivers/Externals/Stc_Drivers/Stc_Drv_PIC32MX575L.c ../Drivers/Externals/UART_Drivers/UART_Drv_PIC32MX575L.c ../Drivers/Externals/uP_Drivers/uP_Drv_PIC32MX575L.c ../Main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1483240396/CAN_Application.o ${OBJECTDIR}/_ext/155432163/ETH_Application.o ${OBJECTDIR}/_ext/1228546422/Magni_Application.o ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o ${OBJECTDIR}/_ext/1559361094/Rf_Application.o ${OBJECTDIR}/_ext/497683448/Status_Application.o ${OBJECTDIR}/_ext/839880948/UART_Application.o ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/1472/Main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d ${OBJECTDIR}/_ext/155432163/ETH_Application.o.d ${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d ${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d ${OBJECTDIR}/_ext/497683448/Status_Application.o.d ${OBJECTDIR}/_ext/839880948/UART_Application.o.d ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d ${OBJECTDIR}/_ext/1472/Main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1483240396/CAN_Application.o ${OBJECTDIR}/_ext/155432163/ETH_Application.o ${OBJECTDIR}/_ext/1228546422/Magni_Application.o ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o ${OBJECTDIR}/_ext/1559361094/Rf_Application.o ${OBJECTDIR}/_ext/497683448/Status_Application.o ${OBJECTDIR}/_ext/839880948/UART_Application.o ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o ${OBJECTDIR}/_ext/1472/Main.o

# Source Files
SOURCEFILES=../Application/Externals/CAN_Application/CAN_Application.c ../Application/Externals/Ethernet_Application/ETH_Application.c ../Application/Externals/Magni_Application/Magni_Application.c ../Application/Externals/Pcs_Application/Pcs_Application.c ../Application/Externals/Rf_Application/Rf_Application.c ../Application/Externals/Status_Application/Status_Application.c ../Application/Externals/UART_Application/UART_Application.c ../Drivers/Externals/4to20mA_Drivers/4to20mA_Drv_PIC32MX575L.c ../Drivers/Externals/CAN_Drivers/CAN_Drv_PIC32MX575L.c ../Drivers/Externals/Ethernet_Drivers/ETH_Drv_PIC32MX575L.c ../Drivers/Externals/Ethernet_Drivers/PBP_ENCX24J600.c ../Drivers/Externals/GPIO_Drivers/GPIO_Drv_PIC32MX575L.c ../Drivers/Externals/Magni_Drivers/Magni_Drv_PIC32MX575L.c ../Drivers/Externals/Rf_Drivers/Rf_Drv_RS9110eth.c ../Drivers/Externals/Stc_Drivers/Stc_Drv_PIC32MX575L.c ../Drivers/Externals/UART_Drivers/UART_Drv_PIC32MX575L.c ../Drivers/Externals/uP_Drivers/uP_Drv_PIC32MX575L.c ../Main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

# The following macros may be used in the pre and post step lines
Device=PIC32MX575F256L
ProjectDir="C:\Workspace\MPLABX\Proxi-Com_M3R\_MPLAB"
ConfName=WithBootloader
ImagePath="dist\WithBootloader\${IMAGE_TYPE}\_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ImageDir="dist\WithBootloader\${IMAGE_TYPE}"
ImageName="_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IsDebug="true"
else
IsDebug="false"
endif

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE} ${MAKE_OPTIONS} -f nbproject/Makefile-WithBootloader.mk dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
	@echo "--------------------------------------"
	@echo "User defined post-build step: [python crcfromhex.py]"
	@python crcfromhex.py
	@echo "--------------------------------------"

MP_PROCESSOR_OPTION=32MX575F256L
MP_LINKER_FILE_OPTION=,--script="flashWithBootloader_v0.1.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1483240396/CAN_Application.o: ../Application/Externals/CAN_Application/CAN_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1483240396 
	@${RM} ${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1483240396/CAN_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d" -o ${OBJECTDIR}/_ext/1483240396/CAN_Application.o ../Application/Externals/CAN_Application/CAN_Application.c   
	
${OBJECTDIR}/_ext/155432163/ETH_Application.o: ../Application/Externals/Ethernet_Application/ETH_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/155432163 
	@${RM} ${OBJECTDIR}/_ext/155432163/ETH_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/155432163/ETH_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/155432163/ETH_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/155432163/ETH_Application.o.d" -o ${OBJECTDIR}/_ext/155432163/ETH_Application.o ../Application/Externals/Ethernet_Application/ETH_Application.c   
	
${OBJECTDIR}/_ext/1228546422/Magni_Application.o: ../Application/Externals/Magni_Application/Magni_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1228546422 
	@${RM} ${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1228546422/Magni_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d" -o ${OBJECTDIR}/_ext/1228546422/Magni_Application.o ../Application/Externals/Magni_Application/Magni_Application.c   
	
${OBJECTDIR}/_ext/1626443996/Pcs_Application.o: ../Application/Externals/Pcs_Application/Pcs_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1626443996 
	@${RM} ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d" -o ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o ../Application/Externals/Pcs_Application/Pcs_Application.c   
	
${OBJECTDIR}/_ext/1559361094/Rf_Application.o: ../Application/Externals/Rf_Application/Rf_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1559361094 
	@${RM} ${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1559361094/Rf_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d" -o ${OBJECTDIR}/_ext/1559361094/Rf_Application.o ../Application/Externals/Rf_Application/Rf_Application.c   
	
${OBJECTDIR}/_ext/497683448/Status_Application.o: ../Application/Externals/Status_Application/Status_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/497683448 
	@${RM} ${OBJECTDIR}/_ext/497683448/Status_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/497683448/Status_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/497683448/Status_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/497683448/Status_Application.o.d" -o ${OBJECTDIR}/_ext/497683448/Status_Application.o ../Application/Externals/Status_Application/Status_Application.c   
	
${OBJECTDIR}/_ext/839880948/UART_Application.o: ../Application/Externals/UART_Application/UART_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/839880948 
	@${RM} ${OBJECTDIR}/_ext/839880948/UART_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/839880948/UART_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/839880948/UART_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/839880948/UART_Application.o.d" -o ${OBJECTDIR}/_ext/839880948/UART_Application.o ../Application/Externals/UART_Application/UART_Application.c   
	
${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o: ../Drivers/Externals/4to20mA_Drivers/4to20mA_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1089000781 
	@${RM} ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o ../Drivers/Externals/4to20mA_Drivers/4to20mA_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o: ../Drivers/Externals/CAN_Drivers/CAN_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1433678850 
	@${RM} ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o ../Drivers/Externals/CAN_Drivers/CAN_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o: ../Drivers/Externals/Ethernet_Drivers/ETH_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/204936253 
	@${RM} ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o ../Drivers/Externals/Ethernet_Drivers/ETH_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o: ../Drivers/Externals/Ethernet_Drivers/PBP_ENCX24J600.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/204936253 
	@${RM} ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d 
	@${RM} ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d" -o ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o ../Drivers/Externals/Ethernet_Drivers/PBP_ENCX24J600.c   
	
${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o: ../Drivers/Externals/GPIO_Drivers/GPIO_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1449770731 
	@${RM} ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o ../Drivers/Externals/GPIO_Drivers/GPIO_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o: ../Drivers/Externals/Magni_Drivers/Magni_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/142931200 
	@${RM} ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o ../Drivers/Externals/Magni_Drivers/Magni_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o: ../Drivers/Externals/Rf_Drivers/Rf_Drv_RS9110eth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1482869018 
	@${RM} ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d 
	@${RM} ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d" -o ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o ../Drivers/Externals/Rf_Drivers/Rf_Drv_RS9110eth.c   
	
${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o: ../Drivers/Externals/Stc_Drivers/Stc_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2013135956 
	@${RM} ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o ../Drivers/Externals/Stc_Drivers/Stc_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o: ../Drivers/Externals/UART_Drivers/UART_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/674105324 
	@${RM} ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o ../Drivers/Externals/UART_Drivers/UART_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o: ../Drivers/Externals/uP_Drivers/uP_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/9156161 
	@${RM} ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o ../Drivers/Externals/uP_Drivers/uP_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/1472/Main.o: ../Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1472/Main.o.d" -o ${OBJECTDIR}/_ext/1472/Main.o ../Main.c   
	
else
${OBJECTDIR}/_ext/1483240396/CAN_Application.o: ../Application/Externals/CAN_Application/CAN_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1483240396 
	@${RM} ${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1483240396/CAN_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1483240396/CAN_Application.o.d" -o ${OBJECTDIR}/_ext/1483240396/CAN_Application.o ../Application/Externals/CAN_Application/CAN_Application.c   
	
${OBJECTDIR}/_ext/155432163/ETH_Application.o: ../Application/Externals/Ethernet_Application/ETH_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/155432163 
	@${RM} ${OBJECTDIR}/_ext/155432163/ETH_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/155432163/ETH_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/155432163/ETH_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/155432163/ETH_Application.o.d" -o ${OBJECTDIR}/_ext/155432163/ETH_Application.o ../Application/Externals/Ethernet_Application/ETH_Application.c   
	
${OBJECTDIR}/_ext/1228546422/Magni_Application.o: ../Application/Externals/Magni_Application/Magni_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1228546422 
	@${RM} ${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1228546422/Magni_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1228546422/Magni_Application.o.d" -o ${OBJECTDIR}/_ext/1228546422/Magni_Application.o ../Application/Externals/Magni_Application/Magni_Application.c   
	
${OBJECTDIR}/_ext/1626443996/Pcs_Application.o: ../Application/Externals/Pcs_Application/Pcs_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1626443996 
	@${RM} ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1626443996/Pcs_Application.o.d" -o ${OBJECTDIR}/_ext/1626443996/Pcs_Application.o ../Application/Externals/Pcs_Application/Pcs_Application.c   
	
${OBJECTDIR}/_ext/1559361094/Rf_Application.o: ../Application/Externals/Rf_Application/Rf_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1559361094 
	@${RM} ${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1559361094/Rf_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1559361094/Rf_Application.o.d" -o ${OBJECTDIR}/_ext/1559361094/Rf_Application.o ../Application/Externals/Rf_Application/Rf_Application.c   
	
${OBJECTDIR}/_ext/497683448/Status_Application.o: ../Application/Externals/Status_Application/Status_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/497683448 
	@${RM} ${OBJECTDIR}/_ext/497683448/Status_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/497683448/Status_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/497683448/Status_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/497683448/Status_Application.o.d" -o ${OBJECTDIR}/_ext/497683448/Status_Application.o ../Application/Externals/Status_Application/Status_Application.c   
	
${OBJECTDIR}/_ext/839880948/UART_Application.o: ../Application/Externals/UART_Application/UART_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/839880948 
	@${RM} ${OBJECTDIR}/_ext/839880948/UART_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/839880948/UART_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/839880948/UART_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/839880948/UART_Application.o.d" -o ${OBJECTDIR}/_ext/839880948/UART_Application.o ../Application/Externals/UART_Application/UART_Application.c   
	
${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o: ../Drivers/Externals/4to20mA_Drivers/4to20mA_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1089000781 
	@${RM} ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/1089000781/4to20mA_Drv_PIC32MX575L.o ../Drivers/Externals/4to20mA_Drivers/4to20mA_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o: ../Drivers/Externals/CAN_Drivers/CAN_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1433678850 
	@${RM} ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/1433678850/CAN_Drv_PIC32MX575L.o ../Drivers/Externals/CAN_Drivers/CAN_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o: ../Drivers/Externals/Ethernet_Drivers/ETH_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/204936253 
	@${RM} ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/204936253/ETH_Drv_PIC32MX575L.o ../Drivers/Externals/Ethernet_Drivers/ETH_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o: ../Drivers/Externals/Ethernet_Drivers/PBP_ENCX24J600.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/204936253 
	@${RM} ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d 
	@${RM} ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o.d" -o ${OBJECTDIR}/_ext/204936253/PBP_ENCX24J600.o ../Drivers/Externals/Ethernet_Drivers/PBP_ENCX24J600.c   
	
${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o: ../Drivers/Externals/GPIO_Drivers/GPIO_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1449770731 
	@${RM} ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/1449770731/GPIO_Drv_PIC32MX575L.o ../Drivers/Externals/GPIO_Drivers/GPIO_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o: ../Drivers/Externals/Magni_Drivers/Magni_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/142931200 
	@${RM} ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/142931200/Magni_Drv_PIC32MX575L.o ../Drivers/Externals/Magni_Drivers/Magni_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o: ../Drivers/Externals/Rf_Drivers/Rf_Drv_RS9110eth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1482869018 
	@${RM} ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d 
	@${RM} ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o.d" -o ${OBJECTDIR}/_ext/1482869018/Rf_Drv_RS9110eth.o ../Drivers/Externals/Rf_Drivers/Rf_Drv_RS9110eth.c   
	
${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o: ../Drivers/Externals/Stc_Drivers/Stc_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2013135956 
	@${RM} ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/2013135956/Stc_Drv_PIC32MX575L.o ../Drivers/Externals/Stc_Drivers/Stc_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o: ../Drivers/Externals/UART_Drivers/UART_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/674105324 
	@${RM} ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/674105324/UART_Drv_PIC32MX575L.o ../Drivers/Externals/UART_Drivers/UART_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o: ../Drivers/Externals/uP_Drivers/uP_Drv_PIC32MX575L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/9156161 
	@${RM} ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d 
	@${RM} ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o.d" -o ${OBJECTDIR}/_ext/9156161/uP_Drv_PIC32MX575L.o ../Drivers/Externals/uP_Drivers/uP_Drv_PIC32MX575L.c   
	
${OBJECTDIR}/_ext/1472/Main.o: ../Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1472 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -fdata-sections -D_SUPPRESS_PLIB_WARNING -I".." -I"../Application" -I"../Application/Externals/Rf_Application" -I"../Application/Externals/Status_Application" -I"../Application/Externals/CAN_Application" -I"../Application/Externals/UART_Application" -I"../Application/Externals/Pcs_Application" -I"../Application/Externals/Magni_Application" -I"../Application/Internals" -I"../Drivers" -I"../Drivers/Externals/uP_Drivers" -I"../Drivers/Externals/Rf_Drivers" -I"../Drivers/Externals/GPIO_Drivers" -I"../Drivers/Externals/Stc_Drivers" -I"../Drivers/Externals/CAN_Drivers" -I"../Drivers/Externals/UART_Drivers" -I"../Drivers/Externals/4to20mA_Drivers" -I"../Drivers/Externals/Magni_Drivers" -I"../Drivers/Internals" -I"." -I"../Drivers/Externals/Ethernet_Drivers" -I"../Application/Externals/Ethernet_Application" -Wall -MMD -MF "${OBJECTDIR}/_ext/1472/Main.o.d" -o ${OBJECTDIR}/_ext/1472/Main.o ../Main.c   
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    flashWithBootloader_v0.1.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}           -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/lib",-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/pic32mx/lib",-L".",-Map="${DISTDIR}/Proxi-Com_M3R.X.${IMAGE_TYPE}.map"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   flashWithBootloader_v0.1.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/lib",-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/pic32mx/lib",-L".",-Map="${DISTDIR}/Proxi-Com_M3R.X.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/_MPLAB.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/WithBootloader
	${RM} -r dist/WithBootloader

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif

#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../Application/CAN_Application/CAN_Application.c ../Drivers/CAN_Drivers/CAN_Drv_PIC32MX795L.c ../Drivers/uP_Drivers/uP_Drv_PIC32MX795L.c ../Main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1092243571/CAN_Application.o ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o ${OBJECTDIR}/_ext/1472/Main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d ${OBJECTDIR}/_ext/1472/Main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1092243571/CAN_Application.o ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o ${OBJECTDIR}/_ext/1472/Main.o

# Source Files
SOURCEFILES=../Application/CAN_Application/CAN_Application.c ../Drivers/CAN_Drivers/CAN_Drv_PIC32MX795L.c ../Drivers/uP_Drivers/uP_Drv_PIC32MX795L.c ../Main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX575F256L
MP_LINKER_FILE_OPTION=,--script="flashPersistentSettings_v0.1.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1092243571/CAN_Application.o: ../Application/CAN_Application/CAN_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1092243571" 
	@${RM} ${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1092243571/CAN_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d" -o ${OBJECTDIR}/_ext/1092243571/CAN_Application.o ../Application/CAN_Application/CAN_Application.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o: ../Drivers/CAN_Drivers/CAN_Drv_PIC32MX795L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1405221353" 
	@${RM} ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d" -o ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o ../Drivers/CAN_Drivers/CAN_Drv_PIC32MX795L.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o: ../Drivers/uP_Drivers/uP_Drv_PIC32MX795L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/839522170" 
	@${RM} ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d 
	@${RM} ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d" -o ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o ../Drivers/uP_Drivers/uP_Drv_PIC32MX795L.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1472/Main.o: ../Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/1472/Main.o.d" -o ${OBJECTDIR}/_ext/1472/Main.o ../Main.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/1092243571/CAN_Application.o: ../Application/CAN_Application/CAN_Application.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1092243571" 
	@${RM} ${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d 
	@${RM} ${OBJECTDIR}/_ext/1092243571/CAN_Application.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/1092243571/CAN_Application.o.d" -o ${OBJECTDIR}/_ext/1092243571/CAN_Application.o ../Application/CAN_Application/CAN_Application.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o: ../Drivers/CAN_Drivers/CAN_Drv_PIC32MX795L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1405221353" 
	@${RM} ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d 
	@${RM} ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o.d" -o ${OBJECTDIR}/_ext/1405221353/CAN_Drv_PIC32MX795L.o ../Drivers/CAN_Drivers/CAN_Drv_PIC32MX795L.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o: ../Drivers/uP_Drivers/uP_Drv_PIC32MX795L.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/839522170" 
	@${RM} ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d 
	@${RM} ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o.d" -o ${OBJECTDIR}/_ext/839522170/uP_Drv_PIC32MX795L.o ../Drivers/uP_Drivers/uP_Drv_PIC32MX795L.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1472/Main.o: ../Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -fdata-sections -D_SUPPRESS_PLIB_WARNING -D_DISABLE_OPENADC10_CONFIGPORT_WARNING -I".." -I"../Application" -I"../Application/CAN_Application" -I"../Drivers" -I"../Drivers/uP_Drivers" -I"../Drivers/CAN_Drivers" -Wall -MMD -MF "${OBJECTDIR}/_ext/1472/Main.o.d" -o ${OBJECTDIR}/_ext/1472/Main.o ../Main.c    -no-legacy-libc  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    flashPersistentSettings_v0.1.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_ICD3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -no-legacy-libc  $(COMPARISON_BUILD)    -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/lib",-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/pic32mx/lib",-L".",-Map="${DISTDIR}/Proxi-Com_M3R.X.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   flashPersistentSettings_v0.1.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/lib",-L"../../../../../../Program Files (x86)/Microchip/MPLABX C32 Suite/pic32mx/lib",-L".",-Map="${DISTDIR}/Proxi-Com_M3R.X.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/CAN_Transponder.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif

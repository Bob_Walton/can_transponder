#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=CAN_Transponder.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/CAN_Transponder.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=cantransponder.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/cantransponder.x.tar
# WithBootloader configuration
CND_ARTIFACT_DIR_WithBootloader=dist/WithBootloader/production
CND_ARTIFACT_NAME_WithBootloader=CAN_Transponder.X.production.hex
CND_ARTIFACT_PATH_WithBootloader=dist/WithBootloader/production/CAN_Transponder.X.production.hex
CND_PACKAGE_DIR_WithBootloader=${CND_DISTDIR}/WithBootloader/package
CND_PACKAGE_NAME_WithBootloader=cantransponder.x.tar
CND_PACKAGE_PATH_WithBootloader=${CND_DISTDIR}/WithBootloader/package/cantransponder.x.tar

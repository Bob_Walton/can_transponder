import os
import binascii
import struct

path = os.getcwd() + "\\dist\\WithBootloader\\production\\"

hexfile = path + "_MPLAB.production.hex"
hexfile2 = path + "_MPLAB.production_fwupgrade.hex"

try:
    os.remove(hexfile2)
except WindowsError:
    print "File Removed"
    # no file to remove

# hex line data is '8e78f8ff'
# written word is 0xfff8788e (msb->lsb)

f = open(hexfile, 'r')
f2 = open(hexfile2, 'w+')
# empty file
f2.truncate()

hexData = list()

crcCurrent = 0
crcCurrent = long(crcCurrent)

for line in f:
    d = 0
    recType = int(line[7:9], 16)
    numBytes = int(line[1:3], 16)
    if recType == 0:
        for word in range(numBytes / 4):  # loop for program words
            for offset in range(3, -1, -1):  # loop for individual bytes
                index = 9 + (word * 8) + (offset * 2)
                d += int(line[index:index + 2], 16) << (offset * 8)

        hexData.append(d)

for data in hexData:
    crcCurrent += data

crcCurrent = ~crcCurrent + 1

# print (crcCurrent & 0xffffffffffffffff)
print format((crcCurrent & 0xffffffffffffffff), '016x')

# work out checksum by adding values of bytes
checksum = 0
temp = struct.pack('<q', long(crcCurrent))
for byte in temp:
    checksum += int(binascii.hexlify(byte), 16)

checksum += (8 + 9)    # add in type and size fields

checksum &= 0xff
checksum = ~checksum + 1
checksum &= 0xff

f2.write(':08000009' + format((crcCurrent & 0xffffffffffffffff), '016x') + format((checksum & 0xff), '02x') + '\n')

f.seek(0)
for line in f:
    f2.write(line)

f.close()
f2.close()

print "Great Success!"
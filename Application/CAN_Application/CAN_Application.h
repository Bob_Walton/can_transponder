/** *******************************************************************
@file 				CAN_Application.h
@brief 				CAN Application Header
@details      This application manages the CAN bus connection between the
              physical layer interface and the radio application.
              It provides the functions necessary and API connections.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd\n
							Copyright (C) 2014 PowerbyProxi Inc\n
							All rights reserved

*********************************************************************/
#ifndef __CAN_Application_H
  #define __CAN_Application_H

  //*******************************************************************
  //Includes

  //*******************************************************************
  //#Defines

  //*******************************************************************
  //Typedefs
	
  //*******************************************************************
  //External Variables

  //*******************************************************************
  //Function Prototypes
  void CAN_Application(void);

#endif


/** *******************************************************************
@file 				CAN_Application.c
@brief 				CAN Application
@details 			This application manages the CAN bus connection between the
              physical layer interface and the radio application.
              It provides the functions necessary and API connections.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd\n
							Copyright (C) 2014 PowerbyProxi Inc\n
							All rights reserved

*********************************************************************/
#define __CAN_Application_C

//*******************************************************************
//Includes
//Global Layer
#include "Global.h"
#include "AppConfig.h"

//Application Layer
#include "CAN_Application.h"

//Driver Layer
#include "CAN_Drv_PIC32MX795L.h"
#include "uP_Drv_PIC32MX795L.h"

//*******************************************************************
//#Defines
/** @brief CAN states */
typedef enum
{
  CAN_state_init,
  CAN_state_exec
}CAN_state_t;

//*******************************************************************
//Variables
/** @brief CAN state array */
CAN_state_t     CAN_state[CAN_CHANNELS_MAX] = {CAN_state_init};

/** @brief      CAN application channel number*/
uint8_t         CAN_Channel = 0;

//*******************************************************************
//State Machine
//*******************************************************************
/** ******************************************************************
  @brief        State Machine for the CAN application
  @details      This state machine controls the function of the CAN
                for startup through to an online state.

  @pre          Primary link flag needs to be true.
*********************************************************************/
void CAN_Application(void)
{
  switch(CAN_state[CAN_Channel])
  {
    case CAN_state_init:
      if(CAN_Init(CAN_Channel + Intf_CAN_1) != 0)
      {
        CAN_state[CAN_Channel] = CAN_state_exec;                        //move to next state
      }
      break;

    case CAN_state_exec:
      CAN_Transponder(CAN_Channel + Intf_CAN_1);
      break;
  }
}

//*******************************************************************
//End of State Machine
//*******************************************************************

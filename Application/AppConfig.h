/** *******************************************************************
@file 				AppConfig.h
@brief        Defines the application layer API interface
@details      Defines the application configuration for the
              Application Layer. These definitions will tie together
              the various applications for the desired project.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd\n
							Copyright (C) 2014 PowerbyProxi Inc\n
							All rights reserved

*********************************************************************/
#ifndef __AppConfig_H
  #define __AppConfig_H

  //*******************************************************************
  //Includes

  //*******************************************************************
  //#Defines

  //*******************************************************************
  //Typedefs

  //*******************************************************************
  //Variables

  //*******************************************************************
  //Function Prototypes

#endif

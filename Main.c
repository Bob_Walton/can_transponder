/** *******************************************************************
@file 		    Main.c
@brief 		    Main Application
@details 	    This is the application entry point and defines items necessary
			        to enter the Main Function. The main function should never complete
			        or exit except in the event that an exception occurs. Any exceptions
			        should be trapped and dealt with appropriately.

@copyright	  Copyright (C) 2014 PowerbyProxi Ltd\n
			        Copyright (C) 2014 PowerbyProxi Inc\n
			        All rights reserved

*********************************************************************/
#define __Main_C

/** @brief  Microchip PIC32MX795L uP fuses which have to be set before includes*/
#define __MICROCHIPFUSES

//*******************************************************************
//Includes
//Global Layer
#include "Global.h"

//Application Layer
#include "CAN_Application.h"

//Driver Layer
#include "uP_Drv_PIC32MX795L.h"

//*******************************************************************
//TODO


//*******************************************************************
//#Defines

//*******************************************************************
//Typedefs
//*******************************************************************

//*******************************************************************
//Variables

//*******************************************************************
//Functions
//*******************************************************************

/** ******************************************************************
@brief 			Application Entry Point
*********************************************************************/
int main(void)
{
  if(uP_SystemInit())                                                 // Initialize the uP hardware and application settings
  {
    while(True);                                                      // error initializing system
  }

  LED_ChangeStatus(True);                                             // turn LED on

  while(True)                                                         // program loop
  {
    CAN_Application();                                                // process the CAN application
  }
}//End of Main

/** *******************************************************************
@file         Global.h
@brief        Project global definitions
@details      This file provides definitions which apply to all layers
              and can be accessed by any part of the project.

@copyright    Copyright (C) 2014 PowerbyProxi Ltd\n
              Copyright (C) 2014 PowerbyProxi Inc\n
              All rights reserved

*********************************************************************/
#ifndef __Global_H
  #define __Global_H

  //*******************************************************************
  //#Include
  #include "string.h"

  //*******************************************************************
  //#Defines
  #define _SysUses_CAN

//*******************************************************************
  //Product settings
  #define Product_Version_Software_Default      "V01.00"
  #define Product_Description_Short_Default     "CAN Transponder"
  #define Project_Name_Default                  "No Name"
  #define Project_BuildDate_Default             "29/06/16"
  #define Project_Serial_Default                "No SNo Assigned"

  //Config structure settings
  #define CFGVERSION                            0x01                  // Version 1 of config settings

  //RF settings
  #define RF_DEFAULT_FLUSHTIME                  20                    // flush time in ms
  #define RF_DEFAULT_MAXPAYLOAD                 1492                  // max size of payload

  #define RF_DEFAULT_SPOOFING                   0                     // 1 = spoofing on, 0 = spoofing off
  #define RF_DEFAULT_SPOOFTIMER                 90                    // timeout to insert a spoof after [ms]
  #define RF_DEFAULT_SPOOFMAX                   3                     // max number of spoofs

  //MAC settings
  #ifdef _RwTesting
    #define Rf_SSID                             "RW_Testing"
    #define RF_DEFAULT_CHANNEL                    1                   //default radio channel
  #elif defined _DhTesting
    #define Rf_SSID                             "Dh_Testing"
    #define RF_DEFAULT_CHANNEL                    1                   //default radio channel
  #elif defined _JGTesting
    #define Rf_SSID                             "JG_Testing"
    #define RF_DEFAULT_CHANNEL                    1                   //default radio channel
  #elif defined _SbTesting
    #define Rf_SSID                             "Sb_Testing"
    #define RF_DEFAULT_CHANNEL                    1                   //default radio channel
  #elif defined _DjTesting
    #define Rf_SSID                             "Dj_Testing"
    #define RF_DEFAULT_CHANNEL                    1                   //default radio channel
  #elif defined _PnTesting
    #define Rf_SSID                             "Pn_Testing"
    #define RF_DEFAULT_CHANNEL                    1                   //default radio channel
  #elif defined _AmTesting
    #define Rf_SSID                             "Am_Testing"
    #define RF_DEFAULT_CHANNEL                    11                  //default radio channel
  #elif defined _LcTesting
    #define Rf_SSID                             "Lc_Testing"
    #define RF_DEFAULT_CHANNEL                    12                   //default radio channel
  #else
    #define Rf_SSID                             "CAN_Transponder"      //project SSID
    #define RF_DEFAULT_CHANNEL                    13                   //default radio channel
#endif

#define    Default_Addr_MasterMacB4           254
#define    Default_Addr_SlaveMacB4            255

//Pcs settings
#ifndef _JGTesting
  #define PCSMAC                              { 0x50, 0x43, 0x53, 0x4D, 0x41, 0x45 }  //(PCSMAC in ASCII) 0x43 -> 0x45
#else
  #define PCSMAC                              { 0x50, 0x43, 0x53, 0x4D, 0x41, 0x44  }  //(PCSMAC in ASCII)
#endif

  //UI default settings
  #define UI_DEFAULT_IS_PAIRED                0                       // Default UI  is not paired
  #define UI_DEFAULT_PAIRED_ID                0                       // Default pair ID is null

  //UART default settings
  #define UART_CHANNELS_MAX                   4                       // number of maximum UART channels
  #define UART_DEFAULT_RS232_CHANNELS         1                       // number of RS232 channels
  #define UART_DEFAULT_RS485_CHANNELS         1                       // number of RS485 channels
  #define UART_DEFAULT_USE_RS422              0                       // 1 if RS422 should be used
  #define UART_DEFAULT_USE_DMX                0                       // 1 if DMX should be used
  #define UART_DEFAULT_BAUDRATE               6                       // default baud rate 2400, 4800, 9600, 19200, 38400, 57600, 115200 (6 in enum)
  #define UART_DEFAULT_BITS                   0                       // default bits 8, 9 (0 in enum)
  #define UART_DEFAULT_PARITY                 2                       // default parity 0 = Even, 1 = Odd, 2 = None
  #define UART_DEFAULT_STOPS                  0                       // default stop bits 1 (1 in enum), 2

  //CAN default settings
  #define CAN_CHANNELS_MAX                    1                       // max number of CAN channels
  #define CAN_DEFAULT_CHANNELS                1                       // number of CAN channels
  #define CAN_DEFAULT_BAUDRATE                5                       // default baud rate 1000000 (5 in enum)
  #define CAN_DEFAULT_AUTO_BAUD               0                       // 1 = auto baud on, 0 = auto baud off

  //GPIO default settings
  #define GPIO_CHANNELS_MAX                   8                       // max number of GPIO channels
  #define GPIO_DEFAULT_CHANNELS_IN            1                       // number of GPI channels
  #define GPIO_DEFAULT_CHANNELS_OUT           1                       // number of GPO channels
  #define GPIO_DEFAULT_IN_INVERTS             {0, 0, 0, 0, 0, 0, 0, 0}// TRUE if input is inverted
  #define GPIO_DEFAULT_OUT_INVERTS            {0, 0, 0, 0, 0, 0, 0, 0}// TRUE if output is inverted

  //Ethernet default settings
  #define ETHERNET_CHANNELS_MAX               2                       // max number of Ethernet channels
  #define ETHERNET_DEFAULT_CHANNELS           1                       // number of Ethernet channels
  #define ETHERNET_DEFAULT_TYPE               0                       // default Ethernet type (See Ethernet_Setup_t)

  //I2C settings
  #define I2CCHANNELS                         2                       // number of I2C channels

  #define UI_I2C_Channel                      0                       //channel index for UI I2C
  #define S_4to20mA_I2C_Channel               1                       //channel index for analog I2C

  //ADC settings
  #define ADCCHANNELS                         1                       // number of ADC channels
  #define ADC_Buffer_Averages                 20                      // # of samples of ADC buuufer to stabalize the final result

  //4-20mA default settings
  #define S4to20CHANNELS_MAX                  8                                 // maximum 4-20mA channels
  #define S4to20_DEFAULT_CHANNELS_IN          1                                 // count of 4-20mA channels in
  #define S4to20_DEFAULT_CHANNELS_OUT         1                                 // count of 4-20mA channels out
  #define INPUT_SCALEFACTOR                   0xFFFl                            // converts floating point to INT32 the "l" on the end ensure this value is a LONG

  // Convert 10bit ADC to 4-20mA ==> values are * 100 so that the result has 2 DPs
  #define INPUT_4to20mA_M                     0 * INPUT_SCALEFACTOR;            //set to 0 to indicate not calibrated M of Y=Mx+C Convert ADC to 4-20mA  with 10 bit ADC
  #define INPUT_4to20mA_C                     0 * INPUT_SCALEFACTOR;            //set to 0 to indicate not calibrated C of Y=Mx+C Convert ADC to 4-20mA  with 10 bit ADC

  // Convert 4-20mA to 12bit DAC
  #define OUTPUT_4to20mA_M                    0 * INPUT_SCALEFACTOR;            //set to 0 to indicate not calibrated M of Y=Mx+C Convert 4-20mA to DAC with 12 bit DAC
  #define OUTPUT_4to20mA_C                    0 * INPUT_SCALEFACTOR;            //set to 0 to indicate not calibrated C of Y=Mx+C Convert 4-20mA to DAC with 12 bit DAC

  //0-10V default settings
  #define S0to10CHANNELS_MAX                  8                                 // maximum 0-10v channels
  #define S0to10_DEFAULT_CHANNELS_IN          1                                 // number of 0-10v channels
  #define S0to10_DEFAULT_CHANNELS_OUT         1                                 // number of 0-10v channels

  // Convert 10bit ADC to 0-10V ==> values are * 100 so that the result has 2 DPs
  #define INPUT_0to10V_M                      0 * INPUT_SCALEFACTOR;            //M of Y=Mx+C Convert ADC to 0-10V  with 10 bit ADC
  #define INPUT_0to10V_C                      0 * INPUT_SCALEFACTOR;            //C of Y=Mx+C Convert ADC to 0-10V  with 10 bit ADC

  // Convert 4-20mA to 12bit DAC
  #define OUTPUT_0to10V_M                     0 * INPUT_SCALEFACTOR;            //M of Y=Mx+C Convert 0-10V to DAC with 12 bit DAC
  #define OUTPUT_0to10V_C                     0 * INPUT_SCALEFACTOR;            //C of Y=Mx+C Convert 0-10V to DAC with 12 bit DAC

  // RF Preiod
  #define UPDATEPERIOD                        19                                // default update period in mS

  //LED Rates & Timers
  #define PHY_FLASHHOLDTIMER                  1500                              // if no PHY flash LED at this rate
  #define UI_LED_TIMEOUT                      500                         			// If there is no UI LED toggle every 500ms

  //debug terminal
  #ifdef __DBCONSOLE
    //call appropriate function to allow debugging
    #define DEBUG_STRING(STRING)              TERM_PutString(STRING)
    #define DEBUG_LINE(LINE)                  TERM_PutLine(LINE)
    #define DEBUG_HEX(P_VAL,CNT)              TERM_PutHex(P_VAL,CNT)
    #define DEBUG_DECIMAL(VAL,LEN)            TERM_PutDecimal(VAL,LEN)
  #endif

  #define Null                                ((void *)0)                       // Null pointer

  //*******************************************************************
  //Typedefs

  /** @brief    System Interfaces*/
  typedef enum
  {
    Intf_Status     = 1,        // status interface
    Intf_RS232_1    = 2,        // rs232 channel 1
    Intf_RS232_2    = 3,        // rs232 channel 2
    Intf_RS485_1    = 4,        // rs485 channel 1
    Intf_RS485_2    = 5,        // rs485 channel 2
    Intf_CAN_1      = 6,        // CAN 1 interface
    Intf_CAN_2      = 7,        // CAN 2 interface
    Intf_Ethernet_1 = 8,        // Ethernet interface 1
    Intf_Ethernet_2 = 9,        // Ethernet interface 2
    //end of RF channels to flush
    Intf_Pcs        = 10,       // Pcs interface
  }__attribute__((packed)) Interfaces_t;

  #define     RedpineMacAddress   1

  /** @brief    Address typedefs*/
  typedef enum
  {
    Redpine_MAC_Byte1              = 0x00,
    Redpine_MAC_Byte2              = 0x23,
    Redpine_MAC_Byte3              = 0xA7,
            
    Default_MAC_Byte1              = 0xA3,                                   // MAC byte 1
    Default_MAC_Byte2              = 0x04,                                   // MAC byte 2

    //Master settings
    Addr_MasterMacB1               = 192, 
    Addr_MasterMacB2               = 168,

    #ifdef _SysUses_UI
      Addr_MasterMacB3             = 0,
      Addr_MasterMacB4             = 0,
    #else
      #ifdef _RwTesting
        Addr_MasterMacB3             = 9,
        Addr_MasterMacB4             = 48,
      #elif defined _DhTesting
        Addr_MasterMacB3             = 56,
        Addr_MasterMacB4             = 111,
      #elif defined _JGTesting
        Addr_MasterMacB3             = 56,
        Addr_MasterMacB4             = 100,
      #elif defined _SbTesting
        Addr_MasterMacB3             = 80,                        //6,
        Addr_MasterMacB4             = Default_Addr_MasterMacB4,  //181,
      #elif defined _DjTesting
        Addr_MasterMacB3             = 20,
        Addr_MasterMacB4             = 181,
      #elif defined _Dj2Testing
        Addr_MasterMacB3             = 21,
        Addr_MasterMacB4             = 181,
      #elif defined _PnTesting
        Addr_MasterMacB3             = 81,
        Addr_MasterMacB4             = 161,
      #elif defined _AmTesting
        Addr_MasterMacB3             = 82,
        Addr_MasterMacB4             = 131,
      #elif defined _LcTesting
        Addr_MasterMacB3             = 12,
        Addr_MasterMacB4             = 112,
      #else
        Addr_MasterMacB3             = 3,
        Addr_MasterMacB4             = 130,
      #endif
    #endif

    //Slave settings
    Addr_SLaveMacB1                  = 192,
    Addr_SlaveMacB2                  = 168,

    #ifdef _SysUses_UI
      Addr_SlaveMacB3                = 0,
      Addr_SlaveMacB4                = 0,
    #else
      #ifdef _RwTesting
        Addr_SlaveMacB3              = 9,
        Addr_SlaveMacB4              = 49
      #elif defined _DhTesting
        Addr_SlaveMacB3              = 56,
        Addr_SlaveMacB4              = 112
      #elif defined _JGTesting
        Addr_SlaveMacB3              = 56,
        Addr_SlaveMacB4              = 101
      #elif defined _JrTesting
        Addr_SlaveMacB3              = 83,
        Addr_SlaveMacB4              = 122
      #elif defined _SbTesting
        Addr_SlaveMacB3              = 80,                      //6,
        Addr_SlaveMacB4              = Default_Addr_SlaveMacB4  //182
      #elif defined _DjTesting
        Addr_SlaveMacB3              = 20,
        Addr_SlaveMacB4              = 182
      #elif defined _Dj2Testing
        Addr_SlaveMacB3              = 21,
        Addr_SlaveMacB4              = 182
      #elif defined _PnTesting
        Addr_SlaveMacB3             = 81,
        Addr_SlaveMacB4             = 162
      #elif defined _AmTesting
        Addr_SlaveMacB3             = 82,
        Addr_SlaveMacB4             = 132
      #elif defined _LcTesting
        Addr_SlaveMacB3             = 12,
        Addr_SlaveMacB4             = 113
      #else
        Addr_SlaveMacB3              = 3,
        Addr_SlaveMacB4              = 131
      #endif
    #endif
  }__attribute__((packed)) AddressSettings_t;

  /** @brief    Compatibility typedefs*/
  typedef unsigned char           uint8_t;                                                // compiler compatibility
  typedef signed char             int8_t;                                                 // compiler compatibility
  typedef unsigned short          uint16_t;                                               // compiler compatibility
  typedef signed short            int16_t;                                                // compiler compatibility
  typedef unsigned int            uint32_t;                                               // compiler compatibility
  typedef signed int              int32_t;                                                // compiler compatibility
  typedef unsigned long long      uint64_t;                                               // compiler compatibility
  typedef signed long long        int64_t;                                                // compiler compatibility
  typedef enum                    {False, True}__attribute__((packed)) Bool_t;            // compiler compatibility
  typedef enum                    {Clear, Set}__attribute__((packed)) Bit_t;              // compiler compatibility
  typedef enum                    {Off, On}__attribute__((packed)) Logic_t;               // compiler compatibility

  /** @brief    Bits typedefs*/
  typedef enum
  {
    BIT_0   = 0x01,                                                     // the value of bit 0
    BIT_1   = 0x02,                                                     // the value of bit 1
    BIT_2   = 0x04,                                                     // the value of bit 2
    BIT_3   = 0x08,                                                     // the value of bit 3
    BIT_4   = 0x10,                                                     // the value of bit 4
    BIT_5   = 0x20,                                                     // the value of bit 5
    BIT_6   = 0x40,                                                     // the value of bit 6
    BIT_7   = 0x80,                                                     // the value of bit 7
    BIT_8   = 0x0100,                                                   // the value of bit 8
    BIT_9   = 0x0200,                                                   // the value of bit 9
    BIT_10  = 0x0400,                                                   // the value of bit 10
    BIT_11  = 0x0800,                                                   // the value of bit 11
    BIT_12  = 0x1000,                                                   // the value of bit 12
    BIT_13  = 0x2000,                                                   // the value of bit 13
    BIT_14  = 0x4000,                                                   // the value of bit 14
    BIT_15  = 0x8000                                                    // the value of bit 15
  }__attribute__((packed)) Bits_Def_t;

  //System structures setup
  //Ring buffer sizes
  typedef enum
  {
    SB_CAN  = 3072,
    SB_UART = 2048,
    SB_ETH  = 6144
  }__attribute__((packed)) SysBuffers_t;

  //Ethernet speed
  typedef enum                                                          // Ethernet setup
  {
    ETH_AutoNegotiation = 0,
    ETH_10MBPS_HALF_DUPLEX,
    ETH_10MBPS_FULL_DUPLEX,
    ETH_100MBPS_HALF_DUPLEX,
    ETH_100MBPS_FULL_DUPLEX
  }__attribute__((packed)) Ethernet_Mode_t;

  //UART baud rate
  typedef enum                                                          // UART baud rate
  {
    UART_2400     = 0,
    UART_4800     = 1,
    UART_9600     = 2,
    UART_19200    = 3,
    UART_38400    = 4,
    UART_57600    = 5,
    UART_115200   = 6,
    UART_250000   = 7,
  }__attribute__((packed)) UART_BaudRate_t;

  typedef enum                                                          // UART parity
  {
    UART_Even = 0,
    UART_Odd = 1,
    UART_None = 2
  }__attribute__((packed)) UART_Parity_t;

  typedef enum                                                          // UART parity
  {
    UART_Databits_8 = 0,
    UART_Databits_9 = 1
  }__attribute__((packed)) UART_Databit_t;

  typedef enum                                                          // UART parity
  {
    UART_Stopbits_1 = 0,
    UART_Stopbits_2 = 1
  }__attribute__((packed)) UART_Stopbit_t;

  typedef enum                                                          // UART protocol
  {
    UART_RS232 = 0,
    UART_RS485 = 1,
    UART_RS422 = 2
  }__attribute__((packed)) UART_Protocol_t;

  //CAN baud rate
  typedef enum
  {
    CAN_No_Baud_Rate  = 0,
    CAN_125kbs        = 1,
    CAN_250kbs        = 2,
    CAN_500kbs        = 3,
    CAN_1000kbs       = 4,
  }__attribute__((packed)) CAN_BaudRate_t;

  //*******************************************************************
  //#Structures

  //Interfaces setup
  typedef struct                                                        // Interfaces setup
  {
    uint8_t         SLOT_0_PHYID;                                       // ID of PHY in slot 0
    uint8_t         SLOT_1_PHYID;                                       // ID of PHY in slot 1
    Bool_t          RS232_Enable;                                       // Enable or disable RS232
    Bool_t          RS485_Enable;                                       // Enable or disable RS485
    Bool_t          RS422_Enable;                                       // Enable or disable RS422
    Bool_t          UseDMX;                                             // True if DMX used
    Bool_t          CAN_Enable;                                         // Enable or disable CAN
    Bool_t          GPIO_Enable;                                        // Enable or disable GPIO
    Bool_t          Ethernet_Enable;                                    // Enable or disable Ethernet
    Bool_t          mA4to20_Enable;                                     // Enable or disable 4 to 20mA
    Bool_t          v0to10_Enable;                                      // Enable or disable 0 to 10V
    Bool_t          UI_Enable;                                          // Enable or disable UI interface

    uint8_t         RS232_ChannelCount;                                 // Number of channels RS232
    uint8_t         RS485_ChannelCount;                                 // Number of channels RS485
    uint8_t         CAN_ChannelCount;                                   // Number of channels CAN
    uint8_t         Eth_ChannelCount;                                   // Number of channels Ethernet
    uint8_t         GPIO_ChannelCount_IN;                               // Number of channels IN
    uint8_t         GPIO_ChannelCount_OUT;                              // Number of channels OUT
    uint8_t         mA4to20_ChannelCount_IN;                            // Number of channels IN
    uint8_t         mA4to20_ChannelCount_OUT;                           // Number of channels OUT
    uint8_t         v0to10_ChannelCount_IN;                             // Number of channels IN
    uint8_t         v0to10_ChannelCount_OUT;                            // Number of channels OUT
  }__attribute__((packed)) Sys_Interfaces_Setup_t;

  //Project setup
  typedef struct                                                        // Project setup
  {
    uint8_t         ProjectName[20];                                    // stores 20 character project name
    uint8_t         ShortDescription[13];                               // stores 15 character short description
    uint8_t         SerialNumber[15];                                   // serial number
    uint8_t         SoftwareVersion[6];                                 // software version
    uint8_t         BuildDate[8];                                       // build date
  }__attribute__((packed)) Sys_Project_Setup_t;

  //RF setup
  typedef struct                                                        // RF setup
  {
    uint8_t         RadioChannel;                                       // 802.11 channel
    uint16_t        RFFlushTime;                                        // flush time in ms
    uint16_t        RFMaxPayload;                                       // max payload to send over rf
    Bool_t          Spoofing;                                           // True if spoofing on
    uint16_t        SpoofTimer;                                         // sets the spoof input time
    uint8_t         SpoofMaxNo;                                         // max number of spoofs
    uint16_t        StatusPeriod;                                       // update interval for status
  }__attribute__((packed)) Sys_RF_Setup_t;

  typedef struct
  {
    Bool_t          IsUIConnected;                                      // TRUE when UI connected to proxi-com
    Bool_t          IsRFIDset;                                          // TRUE when RF ID received via UI
    uint16_t        PairID;
  }__attribute__((packed)) Sys_UI_Setup_t;

  //Address settings
  typedef struct                                                        // system dependent structure used to contain address information
  {
    uint8_t         MyMACAddr[6];                                       // application my MAC address
    uint8_t         DestMACAddr[6];                                     // destination MAC address
    uint8_t         SSID[32];                                           // SSID
    Bool_t          IsMaster;                                           // TRUE when this unit is the master
  }__attribute__((packed)) Sys_Address_Setup_t;

  //UART setup
  typedef struct                                                        // UART setup
  {
    UART_BaudRate_t BaudRate;                                           // 2400, 4800, 9600, 19200, 38400, 57600, 115200
    UART_Databit_t  DataBits;                                           // 8, 9
    UART_Parity_t   Parity;                                             // 1 = Even, 2 = Odd, 3 = None
    UART_Stopbit_t  StopBits;                                           // 1, 2
  }__attribute__((packed)) Sys_UART_Setup_t;

  //GPIO setup
  typedef struct                                                        // GPIO setup
  {
    Bool_t In_Invert;                                                   // TRUE if input inverted
    Bool_t Out_Invert;                                                  // TRUE if output inverted
  }__attribute__((packed)) Sys_GPIO_Setup_t;

  //CAN setup
  typedef struct                                                        // CAN setup
  {
    CAN_BaudRate_t  BaudRate;                                           // 50000, 250000, 500000, 1000000
    Bool_t          AutoBaud;                                           // True if auto baud on
  }__attribute__((packed)) Sys_CAN_Setup_t;

  //Ethernet setup
  typedef struct                                                        // Ethernet setup
  {
    Ethernet_Mode_t Mode;                                               // Ethernet mode
  }__attribute__((packed)) Sys_Ethernet_Setup_t;

  //4-20mA settings
  typedef struct                                                        // 4-20mA setup
  {
    int32_t         Input_M;                                            // M of Y=Mx+C
    int32_t         Input_C;                                            // C of Y=Mx+C
    int32_t         Output_M;                                           // M of Y=Mx+C
    int32_t         Output_C;                                           // C of Y=Mx+C
  }__attribute__((packed)) Sys_mA4to20_Setup_t;

  //0-10V settings
  typedef struct                                                        // 0-10V setup
  {
    int32_t         Input_M;                                            // M of Y=Mx+C
    int32_t         Input_C;                                            // C of Y=Mx+C
    int32_t         Output_M;                                           // M of Y=Mx+C
    int32_t         Output_C;                                           // C of Y=Mx+C
  }__attribute__((packed)) Sys_v0to10_Setup_t;

  /** @brief    System settings structure
      @details  The system settings structure is available to all applications
                and drivers within project. If the settings are empty following
                a re-flash of the project then a set of defaults are created. PCS
                has the ability to modify these settings if required to add project
                names, serial numbers or to carry out calibrations.
   */
  typedef struct                                                        // settings to store
  {
    uint8_t                 CfgVersion;                                 // Config structure version
    Sys_Address_Setup_t     AddressSettings;                            // address settings
    Sys_RF_Setup_t          RF_Settings;                                // RF settings
    Sys_UI_Setup_t          UI_Settings;                                // UI specific settings
    Sys_Interfaces_Setup_t  InterfacesSettings;                         // Enable/Disable interfaces and channel counts
    Sys_Project_Setup_t     ProjectSettings;                            // Project settings
    Sys_UART_Setup_t        UART_Setups[UART_CHANNELS_MAX];             // UART settings
    Sys_CAN_Setup_t         CAN_Setups[CAN_CHANNELS_MAX];               // CAN settings
    Sys_Ethernet_Setup_t    EthernetSetup[ETHERNET_CHANNELS_MAX];       // Ethernet settings
    Sys_GPIO_Setup_t        GPIOSetup[GPIO_CHANNELS_MAX];               // GPIO Setup
    Sys_mA4to20_Setup_t     mA4to20Setup[S4to20CHANNELS_MAX];           // 4-20mA settings
    Sys_v0to10_Setup_t      v0to10Setup[S0to10CHANNELS_MAX];            // 0-10V settings
  }__attribute__((packed)) Sys_SetStruct_t;

  typedef enum
  {
    W1R_IDLE_STATUS       = 0,
    W1R_PAIRING_STATUS    = 1,
    W1R_NORMAL_STATUS     = 2,
    W1R_RF_ERROR_STATUS   = 3,
    W1R_PHY_ERRORE_STATUS = 4,
  } System_Status_t;

  //*******************************************************************
  //Variables

  /** @brief    System Settings Structure
      @details  System structure is located at global level which makes
      members accessible to all. Access with APIs through pointers.
   */
  Sys_SetStruct_t SysSettings;

  System_Status_t System_Status;

#endif

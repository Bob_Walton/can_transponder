/** *******************************************************************
@file 				HardwareConfig.h
@brief 				Defines the driver layer hardware interface
@details 			Defines the hardware configuration for the
							Drivers Layer. These definitions will tie together
							the various drivers for the desired project.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd\n
							Copyright (C) 2014 PowerbyProxi Inc\n
							All rights reserved

*********************************************************************/
#ifndef __HardwareConfig_H
  #define __HardwareConfig_H

  //*******************************************************************
  //Includes
  #include "uP_Drv_PIC32MX795L.h"
  #include "Global.h"
  
  //*******************************************************************
  //#Defines
  // Hardware mappings
  #define LED_Port_Direction              TRISCbits.TRISC14         //LED port direction
  #define LED_Port                        PORTCbits.RC14            //LED access
  #define LEDAUX_Port_Direction           TRISDbits.TRISD9          //Aux LED port direction
  #define LEDAUX_Port                     PORTDbits.RD9             //Aux LED access

  // Master/Slave
  #define MasterSlave_Port_Direction      TRISDbits.TRISD14         //Master/Slave port direction
  #define MasterSlave_Port                PORTDbits.RD14            //Master/Slave access

  // RS232 Port1 => UART 2 A
  #define UART_Port1                      UART2A                    // PLib Access name
  #define UART_Port1_Vector               _UART_2A_VECTOR           // Interrupt vector for ISR

  // RS232 Port2 => UART 2 B
  #define UART_Port2                      UART2B                    // PLib Access name
  #define UART_Port2_Vector               _UART_2B_VECTOR           // Interrupt vector for ISR

  // RS485 Port1 => UART 3 A
  #define UART_Port3                      UART3A                    // PLib Access name
  #define UART_Port3_Vector               _UART_3A_VECTOR           // Interrupt vector for ISR
  #define UART_Port3_Dir_Channel          IOPORT_C                  // RS485 Direction Pin
  #define UART_Port3_Dir_Bit              BIT_1                     // Out bit
  #define UART_Port3_Channel              IOPORT_F                  // Channel of RX pin
  #define UART_Port3_TX_Bit               BIT_5                     // Bit of TX pin
  #define UART_Port3_FERR                 U2STAbits.FERR            // Frame Error Bit

  // RS485 Port2 => UART 3 B
  #define UART_Port4                      UART3B                    // PLib Access name
  #define UART_Port4_Vector               _UART_3B_VECTOR           // Interrupt vector for ISR
  #define UART_Port4_Dir_Channel          IOPORT_C                  // RS485 Direction Pin
  #define UART_Port4_Dir_Bit              BIT_2                     // Out bit

  // CAN Port 1
  #define CAN_Port1                       CAN1                      // PLib Access name
  #define CAN_Port1_VECTOR                _CAN_1_VECTOR             // Interrupt vector for ISR
  #define CAN_Port1_CHANNEL_RX            CAN_CHANNEL1              // Channel RX
  #define CAN_Port1_CHANNEL_TX            CAN_CHANNEL0              // Channel TX
  #define CAN_Port1_CHANNEL_RX_EVENT      CAN_CHANNEL1_EVENT        // Channel RX Event
  #define CAN_RxPort_Direction            TRISFbits.TRISF0          // CAN Rx port direction
  #define CAN_TxPort_Direction            TRISFbits.TRISF1          // CAN Tx port direction
  #define CAN_RxPort                      PORTFbits.RF0             // CAN Rx port
  #define CAN_TxPort                      PORTFbits.RF1             // CAN Tx port

  // GPIO Channel 1
  #define GPIO_Channel1_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel1_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel1_IN_BIT            BIT_1                     // In bit
  #define GPIO_Channel1_OUT_BIT           BIT_0                     // Out bit
  // GPIO Channel 2
  #define GPIO_Channel2_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel2_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel2_IN_BIT            BIT_2                     // In bit
  #define GPIO_Channel2_OUT_BIT           BIT_1                     // Out bit
  // GPIO Channel 3
  #define GPIO_Channel3_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel3_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel3_IN_BIT            BIT_3                     // In bit
  #define GPIO_Channel3_OUT_BIT           BIT_2                     // Out bit
  // GPIO Channel 4
  #define GPIO_Channel4_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel4_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel4_IN_BIT            BIT_4                     // In bit
  #define GPIO_Channel4_OUT_BIT           BIT_3                     // Out bit
  // GPIO Channel 5
  #define GPIO_Channel5_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel5_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel5_IN_BIT            BIT_5                     // In bit
  #define GPIO_Channel5_OUT_BIT           BIT_4                     // Out bit
  // GPIO Channel 6
  #define GPIO_Channel6_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel6_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel6_IN_BIT            BIT_6                     // In bit
  #define GPIO_Channel6_OUT_BIT           BIT_5                     // Out bit
  // GPIO Channel 7
  #define GPIO_Channel7_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel7_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel7_IN_BIT            BIT_7                     // In bit
  #define GPIO_Channel7_OUT_BIT           BIT_6                     // Out bit
  // GPIO Channel 8
  #define GPIO_Channel8_IN                IOPORT_D                  // PLib Access name
  #define GPIO_Channel8_OUT               IOPORT_E                  // PLib Access name
  #define GPIO_Channel8_IN_BIT            BIT_8                     // In bit
  #define GPIO_Channel8_OUT_BIT           BIT_7                     // Out bit

  // 4 to 20mA Support
  //I2C bus 4 to 20mA
  #define I2C_Port_4to20mA                I2C1                      // PLib Access name
  // ADC Converters IIC Setup  (With Address Line = 1)
  #define S4TO20MA_I2C_ADDRESS            0xC2

  // 4 to 20mA Channel 1
  #define S4TO20MA_Channel1_ADC_NO        8                         // ADC Channel number input
  #define S4TO20MA_Channel1_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel1_OUT_BIT       BIT_0                     // Out bit

  // 4 to 20mA Channel 2
  #define S4TO20MA_Channel2_ADC_NO        9                         // ADC Channel number input
  #define S4TO20MA_Channel2_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel2_OUT_BIT       BIT_1                     // Out bit

  // 4 to 20mA Channel 3
  #define S4TO20MA_Channel3_ADC_NO        10                        // ADC Channel number input
  #define S4TO20MA_Channel3_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel3_OUT_BIT       BIT_2                     // Out bit

  // 4 to 20mA Channel 4
  #define S4TO20MA_Channel4_ADC_NO        11                        // ADC Channel number input
  #define S4TO20MA_Channel4_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel4_OUT_BIT       BIT_3                     // Out bit

  // 4 to 20mA Channel 5
  #define S4TO20MA_Channel5_ADC_NO        12                        // ADC Channel number input
  #define S4TO20MA_Channel5_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel5_OUT_BIT       BIT_4                     // Out bit

  // 4 to 20mA Channel 6
  #define S4TO20MA_Channel6_ADC_NO        13                        // ADC Channel number input
  #define S4TO20MA_Channel6_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel6_OUT_BIT       BIT_5                     // Out bit

  // 4 to 20mA Channel 7
  #define S4TO20MA_Channel7_ADC_NO        14                        // ADC Channel number input
  #define S4TO20MA_Channel7_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel7_OUT_BIT       BIT_6                     // Out bit

  // 4 to 20mA Channel 8
  #define S4TO20MA_Channel8_ADC_NO        15                        // ADC Channel number input
  #define S4TO20MA_Channel8_OUT           IOPORT_A                  // PLib Access name
  #define S4TO20MA_Channel8_OUT_BIT       BIT_7                     // Out bit

  // Redpine Signals Module on SPI1A slot with INT1
  #define RF_CS_TRIS                      TRISAbits.TRISA9          // set chip select direction
  #define RF_CS_IO                        LATAbits.LATA9            // set chip select
  #define RF_RESET_TRIS                   TRISCbits.TRISC13         // set reset select direction
  #define RF_RESET_IO                     LATCbits.LATC13           // set reset select
  #define RF_POWER_ON                     0                         // power on macro
  #define RF_POWER_OFF                    1                         // power off macro

  #define RF_SCK_TRIS                     TRISDbits.TRISD15         // SPI1A SCK direction
  #define RF_SDI_TRIS                     TRISFbits.TRISF2          // SPI1A SDI direction
  #define RF_SDO_TRIS                     TRISFbits.TRISF8          // SPI1A SDO direction

  #define RF_SSPBUF                       SPI1ABUF                  // buffer register
  #define RF_SPISTAT                      SPI1ASTAT                 // stats register
  #define RF_SPISTATbits                  SPI1ASTATbits             // stats bits
  #define RF_SPICON1                      SPI1ACON                  // configuration register
  #define RF_SPICON1bits                  SPI1ACONbits              // configuration bits
  #define RF_SPI_BRG                      SPI1ABRG                  // clock setting
  #define RF_SPI_IPL                      IPC6bits.SPI1AIP          // interrupt priority
  #define RF_SPI_ISL                      IPC6bits.SPI1AIS          // interrupt sub priority

  #define RF_SPI_RIE                      IEC0bits.SPI1ARXIE        // rx interrupt enable
  #define RF_SPI_TIE                      IEC0bits.SPI1ATXIE        // tx interrupt enable
  #define RF_SPI_EIE                      IEC0bits.SPI1AEIE         // error interrupt enable
  #define RF_SPI_RIF                      IFS0bits.SPI1ARXIF        // rx interrupt flag
  #define RF_SPI_TIF                      IFS0bits.SPI1ATXIF        // tx interrupt flag
  #define RF_SPI_EIF                      IFS0bits.SPI1AEIF         // error interrupt flag

  #define RF_INT_TRIS                     TRISEbits.TRISE8          // INT 1
  #define RF_INT_IO                       PORTEbits.RE8             // IO
  #define RF_INT_EDGE                     INTCONbits.INT1EP         // Edge
  #define RF_INT_IE                       IEC0bits.INT1IE           // Enable
  #define RF_INT_IF                       IFS0bits.INT1IF           // FLag
  #define RF_INT_IPL                      IPC1bits.INT1IP           // Priority
  #define RF_INT_IPS                      IPC1bits.INT1IS           // Sub-Priority

  // I2C
  #define I2C2_SDA_TRIS                    TRISAbits.TRISA15         // Silicon Errata code -> Configure SDA prior to turning on the IIC Module TRIS & LAT => 0
  #define I2C2_SDA                         LATAbits.LATA15           //

  // Magni Support
  #define WF_POWER_TRIS                   TRISGbits.TRISG15         //
  #define WF_POWER_IO                     LATGbits.LATG15           //
  #define WF_POWER_ON                     0                         //
  #define WF_POWER_OFF                    1                         //

  #define WF_POWER_INT_TRIS               TRISAbits.TRISA14         // IPT Power interrupt direction
  #define WF_POWER_IPT_IO                 LATAbits.LATA14           // IPT Power interrupt Pin
  #define WF_CAN_STANDBY2_TRIS            TRISAbits.TRISA0          // CAN stand by #2 direction
  #define WF_CAN_STANDBY2                 LATAbits.LATA0            // CAN stand by #2
  #define WF_LOADSHDN2_TRIS               TRISAbits.TRISA1          // Load shutdown #2 direction
  #define WF_LOADSHDN2                    LATAbits.LATA1            // Load shutdown #2
  #define WF_SHORTCIRCUIT_TRIS            TRISEbits.TRISE4          // short circuit protection direction
  #define WF_SHORTCIRCUIT                 LATEbits.LATE4            // short circuit protection
  #define WF_RESET_SHORTCIRCUIT_ON        0                         // start reset the short circuit protection
  #define WF_RESET_SHORTCIRCUIT_OFF       1                         // stop reset the short circuit protection

  #define OTP_BUCK_TRIS                   TRISDbits.TRISD12         // Over Temperature Buck Boost enable/disable direction
  #define OTP_BUCK_IO                     LATDbits.LATD12           // Over Temperature Buck Boost enable/disable
  #define OTP_REF_OUT_TRIS                TRISBbits.TRISB10         // Over Temperature Reference output direction
  #define OTP_REF_OUT_IO                  LATBbits.LATB10           // Over Temperature Reference output
  #define OTP_ADC_IN_TRIS                 TRISBbits.TRISB4          // Over Temperature Reference output direction

  #define WF_BATTERY_PORT                 IOPORT_D                  // Battery charged detection direction
  #define WF_BATTERY_BIT                  BIT_11                    // Battery charged detection

  //I2C bus Magni
  #define I2C_Port_Magni                  I2C2                      // PLib Access name

  //ADC Magni
  #define ADC_Port                        ENABLE_AN4_ANA            // PLib Access name
  #define ADC_Channel1                    4                         // channel number
  #define ADC_Config1                     ADC_MODULE_ON | ADC_IDLE_CONTINUE | ADC_FORMAT_INTG | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON
  #define ADC_Config2                     ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_ON | ADC_SAMPLES_PER_INT_16 | ADC_BUF_16 | ADC_ALT_INPUT_OFF
  #define ADC_Config3                     ADC_SAMPLE_TIME_31 | ADC_CONV_CLK_PB | ADC_CONV_CLK_8Tcy
  #define ADC_Configscan                  SKIP_SCAN_AN0 | SKIP_SCAN_AN1 | SKIP_SCAN_AN2 | SKIP_SCAN_AN3 | SKIP_SCAN_AN5 | SKIP_SCAN_AN6 | SKIP_SCAN_AN7 | SKIP_SCAN_AN8 | SKIP_SCAN_AN9 | SKIP_SCAN_AN10 | SKIP_SCAN_AN11 | SKIP_SCAN_AN12 | SKIP_SCAN_AN13 | SKIP_SCAN_AN14 | SKIP_SCAN_AN15
  #define ADC_Channel_Config              ADC_CH0_NEG_SAMPLEA_NVREF | ADC_CH0_POS_SAMPLEA_AN4     // A/D Channel 0 negative input select for SAMPLE B | A/D Chan 0 pos input select for SAMPLE A is AN4

  //*******************************************************************
  //Typedefs

  //*******************************************************************
  //External Variables

  //*******************************************************************
  //Function Prototypes

#endif

/** *******************************************************************
@file 				CAN_Drv_PIC32MX795L.c
@brief 				PIC32MX CAN driver.
@details 			Interrupt based CAN implementation. This implementation uses a
              separate RX and TX buffer, which are filled/emptied using interrupts
              for receiving data. Sending data doesn't use an interrupt.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd
							Copyright (C) 2014 PowerbyProxi Inc
							All rights reserved

*********************************************************************/
#define __CAN_Drv_PIC32MX795L_C

//*******************************************************************
//Includes
//Global Layer
#include "Global.h"
#include "HardwareConfig.h"

//Application Layer

//Driver Layer
#include "CAN_Drv_PIC32MX795L.h"

//*******************************************************************
//#Defines
//CAN FIFO
#define CAN_FIFO_NUMBER_OF_CHANNELS   2       // One for TX and one for RX
#define CAN_FIFO_NUMBER_OF_BUFFERS    32      // Up to 32 per channel

#define CAN_Magic_1_Value             0xBE
#define CAN_Magic_2_Value             0xEF
#define CAN_Magic_BAUD1_Value         0xAB
#define CAN_Magic_BAUD2_Value         0xBA

#define CAN_ATTEMPT_TIMEOUT           10      // 10ms per baud rate to find baud rate of system

#define CAN_ALLOWBLOCKED_LIST_LENGTH  194     // CAN allow/blocked list length

//*******************************************************************
//Typedefs

//*******************************************************************
//Prototypes
/** @brief   Sets the CAN baud rate */
uint16_t CAN_Config(uint8_t Ch_index, CAN_BaudRate_t baud, CAN_OP_MODE fromMode, CAN_OP_MODE toMode);

/** @brief   Sets CAN controller operating mode */
void CAN_SetOpMode(uint8_t Ch_index, CAN_OP_MODE configMode);

//*******************************************************************
//Variables
/** @brief  CAN interface array */
uint8_t CANMessageFifoArea[CAN_CHANNELS_MAX][CAN_FIFO_NUMBER_OF_CHANNELS * CAN_FIFO_NUMBER_OF_BUFFERS * CAN_TX_RX_MESSAGE_SIZE_BYTES];

/** @brief  RX buffer memory allocation */
uint8_t CAN_RX_Buffer_Mem[CAN_CHANNELS_MAX][SB_CAN];

/** @brief  TX buffer memory allocation */
uint8_t CAN_TX_Buffer_Mem[CAN_CHANNELS_MAX][SB_CAN];

///** @brief  Allow/blocked list */
//uint32_t CAN_ABList[CAN_CHANNELS_MAX][CAN_ALLOWBLOCKED_LIST_LENGTH];

///** @brief  temporary baud rate to find correct one */
//static CAN_BaudRate_t tempBaud[CAN_CHANNELS_MAX];

//*******************************************************************
//Functions
//*******************************************************************

/** ******************************************************************
@brief 				Initializes CAN module
@details			This function initializes CAN module. It sets up speed, FIFOs,
              filters and interrupts.

@param[in]    index       corresponding CAN channel

@return				Returns 1 if baud rate is valid
*********************************************************************/
uint16_t CAN_Init(uint8_t Ch_index)
{
  uint16_t result = 0;
  uint8_t   A_index;

  A_index = Ch_index - Intf_CAN_1;

  //Initialize RX buffer by setting in&out to 0, linking buffer
  //memory and setting size and packet count.
  CAN_interface[A_index].RX_Buffer.pBuffer = &CAN_RX_Buffer_Mem[A_index][0];
  CAN_interface[A_index].RX_Buffer.inPos = 0;
  CAN_interface[A_index].RX_Buffer.outPos = 0;
  CAN_interface[A_index].RX_Buffer.size    = SB_CAN;
  CAN_interface[A_index].RX_Buffer.byteCount = 0;

  //Initialize TX buffer by setting in&out to 0, linking buffer
  //memory and setting size and packet count.
  CAN_interface[A_index].TX_Buffer.pBuffer = &CAN_TX_Buffer_Mem[A_index][0];
  CAN_interface[A_index].TX_Buffer.inPos = 0;
  CAN_interface[A_index].TX_Buffer.outPos = 0;
  CAN_interface[A_index].TX_Buffer.size    = SB_CAN;
  CAN_interface[A_index].TX_Buffer.byteCount = 0;

//RWTag
//  Rfc_Channels[Ch_index].ChannelNum = Ch_index;                     // set channel number
//  Rfc_Channels[Ch_index].pHalByteCount = &CAN_GetRXPacketCount;     // status IsGetReady function pointer
//  Rfc_Channels[Ch_index].pHalSpaceAvail = &CAN_GetTXBufferSpace;    // status IsPutReady function pointer
//  Rfc_Channels[Ch_index].pHalGetArray = &CAN_GetByteBlock;          // status GetArray function pointer
//  Rfc_Channels[Ch_index].pHalPutArray = &CAN_PutByteBlock;          // status PutArray function pointer
//  Rfc_Channels[Ch_index].ChannelOpen = True;                        // open channel

  //Initialize hardware to default settings
  CAN_RxPort_Direction = 0;                                         // port is output, until module is turned on
  CAN_TxPort_Direction = 0;                                         // port is output, until module is turned on
  CAN_RxPort = 0;                                                   // set port, until module is turned on
  CAN_TxPort = 0;                                                   // set port, until module is turned on

  // CAN port 1
  CAN_interface[A_index].portReg = (uint32_t)CAN_Port1;                 //Setup structure
  CAN_interface[A_index].channel_RX = (uint32_t)CAN_Port1_CHANNEL_RX;
  CAN_interface[A_index].channel_TX = (uint32_t)CAN_Port1_CHANNEL_TX;
  CAN_interface[A_index].channel_RX_EVENT = (uint32_t)CAN_Port1_CHANNEL_RX_EVENT;

  result = ~PORTReadBits(IOPORT_D, BIT_1 | BIT_2 | BIT_3 | BIT_4) >> 1;  //send GPIO state to chose baudrate
  result &= 0x0F;

  switch(result)
  {
    case 0x01:
      SysSettings.CAN_Setups[A_index].BaudRate = CAN_125kbs;
      break;

    case 0x02:
      SysSettings.CAN_Setups[A_index].BaudRate = CAN_250kbs;
      break;

    case 0x04:
      SysSettings.CAN_Setups[A_index].BaudRate = CAN_500kbs;
      break;

    case 0x08:
      SysSettings.CAN_Setups[A_index].BaudRate = CAN_1000kbs;
      break;
  }

//  if(SysSettings.CAN_Setups[A_index].AutoBaud)
//  {//Initialize baud rate to none as auto detect needs to determine the actual baud rate
//    CAN_interface[A_index].baudrate = CAN_No_Baud_Rate;
//    //Initialize find baud rate state
//    CAN_interface[A_index].findBaud = CAN_findBaud_Init;
//  }
//  else
//  {//Initialize baud rate
    CAN_interface[A_index].baudrate = SysSettings.CAN_Setups[A_index].BaudRate;
//  }

  /* Step 1: Switch the CAN module
   * ON and switch it to Configuration
   * mode. Wait till the switch is
   * complete */

  CAN_SetOpMode(Ch_index, CAN_CONFIGURATION);

  /* Step 2: Configure the CAN Module Clock. The
   * CAN_BIT_CONFIG data structure is used
   * for this purpose. The propagation segment,
   * phase segment 1 and phase segment 2
   * are configured to have 3TQ. The
   * CANSetSpeed function sets the baud.*/

  result = CAN_Config(Ch_index, CAN_interface[A_index].baudrate, CAN_CONFIGURATION, CAN_CONFIGURATION);

  /* Step 3: Assign the buffer area to the
   * CAN module.
   */

  CANAssignMemoryBuffer(CAN_interface[A_index].portReg, CANMessageFifoArea[A_index], sizeof(CANMessageFifoArea[A_index]));

  /* Step 4: Configure channel 0 for TX and size of
   *328 message buffers with RTR disabled and low medium
   * priority. Configure channel 1 for RX and size
   * of 32 message buffers and receive the full message.
   */

  CANConfigureChannelForTx(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX, CAN_FIFO_NUMBER_OF_BUFFERS, CAN_TX_RTR_DISABLED, CAN_LOW_MEDIUM_PRIORITY);
  CANConfigureChannelForRx(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_RX, CAN_FIFO_NUMBER_OF_BUFFERS, CAN_RX_FULL_RECEIVE);

  /* Step 5: Configure filters and mask.
   * Set up to receive all CAN packets */

  CANConfigureFilter(CAN_interface[A_index].portReg, CAN_FILTER0, 0x000, CAN_EID);
  CANConfigureFilterMask(CAN_interface[A_index].portReg, CAN_FILTER_MASK0, 0x000, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);
  CANLinkFilterToChannel(CAN_interface[A_index].portReg, CAN_FILTER0, CAN_FILTER_MASK0, CAN_interface[A_index].channel_RX);
  CANEnableFilter(CAN_interface[A_index].portReg, CAN_FILTER0, True);

  /* Step 6: Enable interrupt and events. Enable the receive
   * channel not empty event (channel event) and the receive
   * channel event (module event).
   * The interrupt peripheral library is used to enable
   * the CAN interrupt to the CPU. */

  CANEnableChannelEvent(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_RX, CAN_RX_CHANNEL_NOT_EMPTY, True);
  CANEnableModuleEvent(CAN_interface[A_index].portReg, CAN_RX_EVENT, True);

  /* Step 7: Switch the CAN mode
  * to normal mode. */

  if(SysSettings.CAN_Setups[A_index].AutoBaud)
  {
    CAN_SetOpMode(Ch_index, CAN_LISTEN_ONLY);
  }
  else
  {
    CAN_SetOpMode(Ch_index, CAN_NORMAL_OPERATION);
  }

  /* These functions are from interrupt peripheral
   * library. */

  INTSetVectorPriority(INT_VECTOR_CAN(CAN_interface[A_index].portReg), INT_PRIORITY_LEVEL_6);
  INTSetVectorSubPriority(INT_VECTOR_CAN(CAN_interface[A_index].portReg), INT_SUB_PRIORITY_LEVEL_0);
  INTEnable(INT_SOURCE_CAN(CAN_interface[A_index].portReg), INT_ENABLED);

  return(result);
}

/** ******************************************************************
@brief        Sets the CAN baud rate
@details      This function sets the baud rate and its corresponding timing settings

@param[in]    index           corresponding CAN channel
@param[in]    baud            Baud rate
@param[in]    fromMode        mode when entering
@param[in]    toMode          mode when exiting

@return       Returns 1 if baud rate is valid
*********************************************************************/
uint16_t CAN_Config(uint8_t Ch_index, CAN_BaudRate_t baud, CAN_OP_MODE fromMode, CAN_OP_MODE toMode)
{
  uint16_t        result = 0;
  uint32_t        rate;
  CAN_BIT_CONFIG  canBitConfig;
  uint8_t         A_index;

  A_index = Ch_index - Intf_CAN_1;

  if(baud == CAN_No_Baud_Rate)
  {
    return(0);           // return as value invalid
  }

  if(fromMode != CAN_CONFIGURATION)
  {
    CAN_SetOpMode(Ch_index, CAN_CONFIGURATION);
  }

  //set baud parameters
	canBitConfig.phaseSeg1Tq = CAN_BIT_8TQ;
  canBitConfig.phaseSeg2Tq = CAN_BIT_8TQ;
  (baud == CAN_1000kbs)?(canBitConfig.propagationSegTq = CAN_BIT_1TQ):(canBitConfig.propagationSegTq = CAN_BIT_7TQ);
  canBitConfig.sample3Time = True;
  canBitConfig.syncJumpWidth = CAN_BIT_1TQ;
	canBitConfig.phaseSeg2TimeSelect = True;

  switch(baud)
  {
    case CAN_125kbs:
      rate = 125000;
      break;

    case CAN_250kbs:
      rate = 250000;
      break;

    case CAN_500kbs:
      rate = 500000;
      break;

    case CAN_1000kbs:
      rate = 1000000;
      break;

    default:
      return 0;
  }

  CANSetSpeed(CAN_interface[A_index].portReg, &canBitConfig, GetSystemClock(), rate);

  if(toMode != CAN_CONFIGURATION)
  {
    CAN_SetOpMode(Ch_index, toMode);
  }

  result = 1;

  return(result);
}

/** ******************************************************************
@brief        Set operating mode
@details      none

@param[in]    index         corresponding CAN channel
@param[in]    configMode    required mode
*********************************************************************/
void CAN_SetOpMode(uint8_t Ch_index, CAN_OP_MODE configMode)
{
  uint16_t      retry = 0;                                               //retry counter
  uint8_t       A_index;

  A_index = Ch_index - Intf_CAN_1;

  if(configMode == CAN_CONFIGURATION)
  {
    CANEnableModule(CAN_interface[A_index].portReg, False);           // disable module
    CANEnableModule(CAN_interface[A_index].portReg, True);            // enable module
  }

  CANSetOperatingMode(CAN_interface[A_index].portReg, configMode);
  while(CANGetOperatingMode(CAN_interface[A_index].portReg) != configMode)
  {
    retry++;

    if(retry >= 0x1000)
    {
      Reset();                                                      //TODO samira to find better solution
    }
  }
}

/** ******************************************************************
@brief        checks for error
@details      none

@param[in]    index         corresponding CAN channel
*********************************************************************/
void CAN_Check4Error(uint8_t Ch_index)
{
  CAN_ERROR_STATE   result;
  uint8_t           A_index;

  A_index = Ch_index - Intf_CAN_1;

  result = CANGetErrorState(CAN_interface[A_index].portReg);

  if((result & CAN_RX_WARNING_STATE) != 0)
  {
      Nop();
  }

  if((result & CAN_TX_WARNING_STATE) != 0)
  {
      Nop();
  }

  if((result & CAN_RX_BUS_PASSIVE_STATE) != 0)
  {
      Nop();
  }

  if((result & CAN_TX_BUS_PASSIVE_STATE) != 0)
  {
      Nop();
  }

  if((result & CAN_TX_BUS_OFF_STATE) != 0)
  {
      Nop();
  }

  if(result)
  {
    do
    {
      Nop();
      Nop();
    }
    while(1);   //stop here
  }
}

/** ******************************************************************
@brief        Load_29Bit_PID_Address
@details      none
*********************************************************************/
void Load_29Bit_PID_Address(CANTxMessageBuffer *TXMessage, uint32_t Address)
{
  uint32_t dwSID;
  uint32_t dwEID;

  dwSID = (Address >> 18) & 0b11111111111;
  dwEID = (Address >> 0) & 0b111111111111111111;

  TXMessage->msgSID.SID = dwSID;
  TXMessage->msgEID.IDE = 1;
  TXMessage->msgEID.EID = dwEID;
}

/** ******************************************************************
@brief        Send_CAN_Message
@details      none
*********************************************************************/
void Send_CAN_Message(uint8_t Ch_index, CANTxMessageBuffer* pMessageToSend)
{
  uint16_t            RetryCount;
  CANTxMessageBuffer  *message;
  uint8_t             A_index;

  A_index = Ch_index - Intf_CAN_1;

  //check if TX FIFO ready
  message = CANGetTxMessageBuffer(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX);

  if(message == Null) // there is something wrong with the CAN bus and messages can't be sent. (eg No Ack)
  { // Reset the FIFO so that old messages don't come through when the bus gets reconnected.
    CANAbortPendingTx(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX);
    RetryCount = 0;
    while ((CANIsTxAborted(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX) == FALSE) && (RetryCount < 0xffff))
    {
      RetryCount++;
    }

    CANResetChannel(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX);
    RetryCount = 0;
    while ((CANIsChannelReset(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX) != TRUE) && (RetryCount < 0xffff))
    {
      RetryCount++;
    }

    message = CANGetTxMessageBuffer(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX); // Try to send out this message.  Should work this time
  }

  if(message != Null)
  {

    memcpy(message, pMessageToSend, sizeof(CANTxMessageBuffer));

    /* This function lets the CAN module
     * know that the message processing is done
     * and message is ready to be processed. */

    CANUpdateChannel(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX);

    /* Direct the CAN module to flush the
     * TX channel. This will send any pending
     * message in the TX channel. */

    CANFlushTxChannel(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_TX);
  }
}

/** ******************************************************************
@brief        CAN Transponder
@details      none

@param[in]    index         corresponding CAN channel
*********************************************************************/
void CAN_Transponder(uint8_t Ch_index)
{
  uint8_t               A_index;
  uint32_t              t_RxMessageSID;
  uint32_t              t_RxMessageEID;
  uint32_t              t_PID;
  uint8_t               SEQ = 0x01;
  uint8_t               ID = 0x0;
  CANRxMessageBuffer    *message;
  CANRxMessageBuffer    messageCopy;
  CANTxMessageBuffer    MyTxMessage;

  A_index = Ch_index - Intf_CAN_1;

  switch(CAN_State)
  {
    case CAN_Waiting:
    {
      if(CAN_interface[A_index].MsgDetector == True)
      {
//        CAN_interface[A_index].MsgDetector = False;

        message = CANGetRxMessage(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_RX);
        messageCopy = *message;

        t_RxMessageSID = messageCopy.msgSID.SID;                                                                    //extract SID
        if(messageCopy.msgEID.IDE)                                                                                  //if extended ID
        {
          t_RxMessageSID<<=18;                                                                                      //shift SID 18 bits
          t_RxMessageEID = messageCopy.msgEID.EID;                                                                  //extract EID
          t_PID = t_RxMessageSID | t_RxMessageEID;                                                                  //make 29bit PID
        }
        else
        {
          t_PID = t_RxMessageSID;                                                                                   //make 11bit PID
        }

        switch(t_PID)
        {
          case CTS_ID:                                                                                              //received CTS
          {
            CAN_State = CAN_SendData;
            break;
          }

          case PING_PACKET_ID:                                                                                      //received Ping
          {
            CAN_State = CAN_Ping;
            break;
          }
        }

//        CANUpdateChannel(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_RX);
//        INTEnable(INT_SOURCE_CAN(CAN_interface[A_index].portReg), INT_ENABLED);
        (void)message;
      }
      break;
    }

    case CAN_SendData:
    {
      do  //Send data to 0x1CEBD12B
      {
        MyTxMessage.messageWord[0] = 0;
        MyTxMessage.messageWord[1] = 0;
        MyTxMessage.messageWord[2] = 0;
        MyTxMessage.messageWord[3] = 0;

        Load_29Bit_PID_Address(&MyTxMessage,DATA_PACKET_ID);

        MyTxMessage.msgEID.DLC = 8;
        MyTxMessage.data[0] = SEQ;

        MyTxMessage.data[1] = ID;
        ID++;
        MyTxMessage.data[2] = ID;
        ID++;
        MyTxMessage.data[3] = ID;
        ID++;
        MyTxMessage.data[4] = ID;
        ID++;
        MyTxMessage.data[5] = ID;
        ID++;
        MyTxMessage.data[6] = ID;
        ID++;
        MyTxMessage.data[7] = ID;
        ID++;

        while(CANIsActive(CAN_interface[A_index].portReg));
        Send_CAN_Message(Ch_index, &MyTxMessage);

        SEQ++;

      }while(SEQ != 48);


      //Send RTS to 0x1CECD12B
      MyTxMessage.messageWord[0] = 0;
      MyTxMessage.messageWord[1] = 0;
      MyTxMessage.messageWord[2] = 0;
      MyTxMessage.messageWord[3] = 0;

      Load_29Bit_PID_Address(&MyTxMessage,RTS_ID);

      MyTxMessage.msgEID.DLC = 8;

      MyTxMessage.data[0] = 0x10;
      MyTxMessage.data[1] = 0xe5;
      MyTxMessage.data[2] = 0x06;
      MyTxMessage.data[3] = 0xfd;
      MyTxMessage.data[4] = 0xfd;
      MyTxMessage.data[5] = 0x00;
      MyTxMessage.data[6] = 0xd7;
      MyTxMessage.data[7] = 0x00;

      while(CANIsActive(CAN_interface[A_index].portReg));
      Send_CAN_Message(Ch_index, &MyTxMessage);

      CAN_interface[A_index].MsgDetector = False;
      CANUpdateChannel(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_RX);
      INTEnable(INT_SOURCE_CAN(CAN_interface[A_index].portReg), INT_ENABLED);

      CAN_State = CAN_Waiting;

      break;
    }

    case CAN_Ping:
    {
      MyTxMessage.messageWord[0] = 0;
      MyTxMessage.messageWord[1] = 0;
      MyTxMessage.messageWord[2] = 0;
      MyTxMessage.messageWord[3] = 0;

      MyTxMessage.msgSID.SID = 0x73b;
      MyTxMessage.msgEID.IDE = 0x01;
      MyTxMessage.msgEID.EID = 0xd12c;

      Load_29Bit_PID_Address(&MyTxMessage,PONG_PACKET_ID);

      MyTxMessage.msgEID.DLC = 8;

      MyTxMessage.data[0] = 0x01;
      MyTxMessage.data[1] = 0xFF;
      MyTxMessage.data[2] = 0xFF;
      MyTxMessage.data[3] = 0xFF;
      MyTxMessage.data[4] = 0xFF;
      MyTxMessage.data[5] = 0xFF;
      MyTxMessage.data[6] = 0xFF;
      MyTxMessage.data[7] = 0xFF;

      Send_CAN_Message(Ch_index, &MyTxMessage);

      while(CANIsActive(CAN_interface[A_index].portReg));

      CAN_interface[A_index].MsgDetector = False;
      CANUpdateChannel(CAN_interface[A_index].portReg, CAN_interface[A_index].channel_RX);
      INTEnable(INT_SOURCE_CAN(CAN_interface[A_index].portReg), INT_ENABLED);

      CAN_State = CAN_Waiting;

      break;
    }
  }
}

/** ******************************************************************
@brief        ISR via a Vector Table for PIC32MX
@details      none
*********************************************************************/
void __ISR(CAN_Port1_VECTOR, IPL6AUTO)
CAN_Port1_INT(void)   // PLib definition
{
  //variables for message type
  CANRxMessageBuffer  *message;
  CAN_CHANNEL_EVENT   channelEvent;
  uint8_t             CAN_Channel;

  CAN_Channel = SysSettings.InterfacesSettings.CAN_ChannelCount - 1;

  channelEvent = CANGetChannelEvent(CAN_interface[CAN_Channel].portReg, CAN_interface[CAN_Channel].channel_RX);

  if(channelEvent & CAN_RX_CHANNEL_NOT_EMPTY)
  {
    message = (CANRxMessageBuffer *)CANGetRxMessage(CAN_interface[CAN_Channel].portReg, CAN_interface[CAN_Channel].channel_RX);

    if(message == Null)                                               //message is a NULL message
    {
      CANUpdateChannel(CAN_interface[CAN_Channel].portReg, CAN_interface[CAN_Channel].channel_RX);
    }
    else
    {
      CAN_interface[CAN_Channel].MsgDetector = True;
      INTEnable(INT_SOURCE_CAN(CAN_interface[CAN_Channel].portReg), INT_DISABLED);
    }
  }
  INTClearFlag(INT_SOURCE_CAN(CAN_interface[CAN_Channel].portReg));  //clear flag to allow time to empty buffer for prior next interrupt
}

/** *******************************************************************
@file 				CAN_Drv_PIC32MX795L.h
@brief 				PIC32MX CAN driver.
@details 			Interrupt based CAN implementation. This implementation uses a
              separate RX and TX buffer, which are filled/emptied using interrupts
              for receiving data. Sending data doesn't use an interrupt.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd\n
							Copyright (C) 2014 PowerbyProxi Inc\n
							All rights reserved

*********************************************************************/
#ifndef __CAN_Drv_PIC32MX795L_H
  #define __CAN_Drv_PIC32MX795L_H

  //*******************************************************************
  //Includes

  //*******************************************************************
  //#Defines
  /* @brief Number of Magic Bytes*/
  #define CAN_Magic_Size                2

  /* @brief Size of CAN message buffers*/
  #define CAN_TXMESSAGE_SIZE            sizeof(CANTxMessageBuffer)
  #define CAN_RXMESSAGE_SIZE            sizeof(CANRxMessageBuffer)

  //Constants
  static const unsigned int CAN_CTS[8] = {0x11,0x49,0x01,0xFF,0xFF,0x00,0xD7,0x00};
  static const unsigned int CAN_ACK[8] = {0x65,0x59,0x06,0x11,0xFF,0xFF,0xFF,0xFF};
  static const unsigned int CAN_NAK[8] = {0xAA,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

  #define CTS_ID 0x1CFFC0D1
  #define RTS_ID 0x1CFFC12A

  #define DATA_PACKET_ID 0x1CFF002B
  #define PING_PACKET_ID 0x1CFF34D2
  #define PONG_PACKET_ID 0x1CFF332C

  //*******************************************************************
  //Typedefs
  //CAN decode states
  typedef enum
  {
    CAN_DMagic1,
    CAN_DMagic2,
    CAN_DMessage
  }CAN_DecodeState_t;

  typedef enum CAN_State_t
  {
    CAN_Waiting = 0,
    CAN_SendData,
    CAN_Ping
  }t_CAN_State;

  typedef volatile struct
  {
    /** @brief pointer to start of buffer memory (DONT TOUCH)*/
    uint8_t* pBuffer;

    /** @brief position of buffer in*/
    uint16_t inPos;

    /** @brief position of buffer out*/
    uint16_t outPos;

    /** @brief max size of buffer*/
    uint16_t size;

    /** @brief number of bytes in buffer*/
    uint16_t byteCount;

  }CAN_ring_buff_t;

  typedef volatile struct
  {
  /** @brief PLib Access name for PIC32MX*/
  uint32_t portReg;

  /** @brief RX Channel Access name for PIC32MX*/
  uint32_t channel_RX;

  /** @brief TX Channel Access name for PIC32MX*/
  uint32_t channel_TX;

  /** @brief RX Channel Event Access name for PIC32MX*/
  uint32_t channel_RX_EVENT;

  /** @brief TX ring buffer*/
  CAN_ring_buff_t TX_Buffer;

  /** @brief RX ring buffer*/
  CAN_ring_buff_t RX_Buffer;

  /** @brief baud rate*/
  CAN_BaudRate_t baudrate;

  /** @brief Msg detector*/
  Bool_t MsgDetector;

  }CAN_interface_t;

  //*******************************************************************
  //External Variables
  /** @brief  CAN interface array */
  CAN_interface_t   CAN_interface[CAN_CHANNELS_MAX];
  t_CAN_State       CAN_State;

  //*******************************************************************
  //Function Prototypes
  /** @brief   Configures and activates the CAN peripheral. */
  uint16_t CAN_Init(uint8_t Ch_index);

  void CAN_Transponder(uint8_t Ch_index);

#endif

/** *******************************************************************
@file 				uP_Drv_PIC32MX795L.h
@brief 				Hardware driver for the PIC32MX575L
@details 			PIC32MX795L related initiation procedures and generic drivers
              for chip specific hardware such as LEDs.

@copyright		Copyright (C) 2014 PowerbyProxi Ltd\n
							Copyright (C) 2014 PowerbyProxi Inc\n
							All rights reserved

*********************************************************************/
#ifndef __uP_Drv_PIC32MX795L_H
  #define __uP_Drv_PIC32MX795L_H

  //*******************************************************************
  //Includes
  #include "Global.h"

  //uP Specific
  #define COMPILER_MPLAB_C32
  #include <p32xxxx.h>
  #include <plib.h>

  //*******************************************************************
  //#Defines
  /** @brief  Microchip PIC32MX575L uP fuses which have to be set before the start of main*/
  #ifdef __MICROCHIPFUSES
    #pragma config FVBUSONIO    = ON              // USB VBUS_ON Selection bit - OFF=VBUS_ON pin is controlled by the Port Function - ON=VBUS_ON pin is controlled by the USB Module
    #pragma config FUSBIDIO     = ON              // USB USBID Selection bit - OFF USBID pin is controlled by the Port Function - ON USBID pin is controlled by the USB Module
    #pragma config FCANIO       = ON              // CAN IO Pin Selection bit - OFF Alternate CAN IO Pins - ON Default CAN IO Pins
    #pragma config FSRSSEL      = PRIORITY_7      // SRS Select - PRIORITY_0 SRS Interrupt Priority Level 0 -> PRIORITY_7
    #pragma config UPLLEN       = ON              // USB PLL Enable bit - ON, Off
    #pragma config UPLLIDIV     = DIV_1           // USB PLL Input Divider bits - DIV_1,DIV_2,DIV_3,DIV_4,DIV_5,DIV_6,DIV_10,DIV_12
    #pragma config WDTPS        = PS8192          // Watchdog Timer Postscale Select bits - (1:PS2), PS1, PS2 , PS4... PS2048...PS1048576
    #pragma config FCKSM        = CSECMD          // CSECME Clock Switching Enabled, Clock Monitoring Enabled - CSECMD Clock Switching Enabled, Clock Monitoring Disabled - CSDCMD Clock Switching Disabled, Clock Monitoring Disabled
    #pragma config OSCIOFNC     = OFF             // CLKO Enable bit - ON, OFF
    #pragma config IESO         = OFF             // Internal External Switch Over bit - OFF Disabled - ON Enabled
    #pragma config FSOSCEN      = OFF             // Secondary oscillator Enable bit - OFF Disabled - ON Enabled
    #pragma config BWP          = OFF             // Boot Flash Write Protect bit - ON=Enabled - OFF=Disabled
    #pragma config PWP          = OFF             // Program Flash Write Protect bits - PWP512K First 512K, PWP508K First 508K.... etc - OFF Disabled  ON Enabled
    #pragma config ICESEL       = ICS_PGx1        // ICE/ICD Comm Channel Select - ICS_PGx1 ICE pins are shared with PGC1, PGD1 - ICS_PGx2 ICE pins are shared with PGC2, PGD2
    #pragma config FPBDIV       = DIV_1           // PBCLK divider          DIV_1,DIV_2,DIV_4,DIV_8
    #pragma config FNOSC        = PRIPLL          // Oscillator Selection bits  FRC=Fast RC oscillator, FRCPLL=Fast RC oscillator w/ PLL, PRI=Primary oscillator (XT, HS, EC), PRIPLL=Primary oscillator (XT, HS, EC) w/ PLL, SOSC=Secondary oscillator, LPRC=Low power RC oscillator, FRCDIV16=Fast RC oscillator with divide by 16, FRCDIV=Fast RC oscillator with divide
    #pragma config POSCMOD      = HS              // Primary Oscillator bits       EC=EC oscillator, XT=XT oscillator resonator, HS=HS oscillator Crystal, OFF=Disabled
    #pragma config FPLLIDIV     = DIV_10          // PLL Input Divider bits        DIV_1,DIV_2,DIV_3,DIV_4,DIV_5,DIV_6,DIV_10,DIV_12
    #pragma config FPLLMUL      = MUL_15          // PLL Multiplier bits           MUL_15,MUL_16,MUL_17,MUL_18,MUL_19,MUL_20,MUL_21,MUL_24
    #pragma config FPLLODIV     = DIV_1           // PLL Output Divider Value      DIV_1,DIV_2,DIV_4,DIV_8,DIV_16,DIV_32,DIV_64,DIV_256
    #pragma config FWDTEN       = OFF             // Watchdog Timer Enable bit - OFF
    #ifdef __DEBUG
      #pragma config CP         = OFF             // Code Protect Enable bit - OFF=Disabled
      #pragma config DEBUG      = ON              // debugger on
    #else
      #pragma config CP         = ON              // Code Protect Enable bit - ON=Enabled
      #pragma config DEBUG      = OFF             // debugger off
    #endif
  #endif

  #define COMPILER_MPLAB_C32
  #define PTR_BASE                  unsigned long
  #define ROM_PTR_BASE              unsigned long
  #define memcmppgm2ram(a,b,c)      memcmp(a,b,c)
  #define strcmppgm2ram(a,b)        strcmp(a,b)
  #define memcpypgm2ram(a,b,c)      memcpy(a,b,c)
  #define strcpypgm2ram(a,b)        strcpy(a,b)
  #define strncpypgm2ram(a,b,c)     strncpy(a,b,c)
  #define strstrrampgm(a,b)         strstr(a,b)
  #define strlenpgm(a)              strlen(a)
  #define strchrpgm(a,b)            strchr(a,b)
  #define strcatpgm2ram(a,b)        strcat(a,b)
  #define ROM                       const
  #define far
  #define FAR
  #define Reset()                   SoftReset()
  #define ClrWdt()                  (WDTCONSET = _WDTCON_WDTCLR_MASK)

  // using the above defines the Clock Hz is now calc'ed
  #define GetSystemClock()      (((48000000ul / 10ul) * 15ul) / 1ul)    // 72MHz   via PLL  -> "HSPLL"      (((OSC / FPLLIDIV) * FPLLMUL) / FPLLODIV)
  #define GetInstructionClock() (GetSystemClock())
  #define GetPeripheralClock()  (GetSystemClock())                      // Set your divider according to your Peripheral Bus Frequency configuration fuse setting  => FPBDIV fuse
  #define CLOCK_LOWPOWER        4000000					// clock speed for low power internal RC in Hz

  // Let compile time pre-processor calculate the PR1 (period)
  #define PRESCALE              256
  #define TICKS_PER_SECOND_DMX  100000 // 10us
  #define TICKS_PER_SECOND      1000   // 1ms
  #define T1_TICK               (GetSystemClock()/PRESCALE/TICKS_PER_SECOND)
  #define T1_TICK_DMX           (GetSystemClock()/PRESCALE/TICKS_PER_SECOND_DMX)
  #define T1_TICK_LOWPWR        (CLOCK_LOWPOWER/PRESCALE/TICKS_PER_SECOND)
  #define T1_TICK_LOWPWR_DMX    (CLOCK_LOWPOWER/PRESCALE/TICKS_PER_SECOND_DMX)

  //** @brief Flash Emulation
  #define FLASH_SUCCESS                   0                                   // Success in writie or erase E2PROM

  #define NUM_DATA_EE_PAGES               (3)                                 // Total number of pages reserved for the operation
  #define ERASE_WRITE_CYCLE_MAX           (1000)                              // Maximum erase cycle per page
  #define NUMBER_OF_INSTRUCTIONS_IN_PAGE  (1024)                              // number of 32-bit word instructions per page
  #define PIC32MX_PAGE_SIZE               (NUMBER_OF_INSTRUCTIONS_IN_PAGE*4)  // Total page size in bytes

  #define FLASH_PAGE_SIZE                 0x1000
  #define FLASH_ADDR_CRC                  (0x9D040000 - FLASH_PAGE_SIZE)
  #define FLASH_ADDR_SETTINGS             (0x9D040000 - 3 * FLASH_PAGE_SIZE)

  //*******************************************************************
  //Typedefs
  //** @brief Flash EEPROM Emulation Flags for the error/warning conditions.
  typedef enum
  {
    uP_OK,
    EE_EraseFail,
    EE_ReadFail,
    EE_WriteFail,
    uP_EEInitFail,
  }uP_Returns_t;

  typedef enum
  {
    lowPower = 0,
    fullSpeed = 1
  } oscillators_t;

  //*******************************************************************
  //External Variables

  //*******************************************************************
  //Function Prototypes
  uP_Returns_t uP_SystemInit(void);
  void TickInit(void);
  void LED_ChangeStatus(Bit_t status);
  void LED_Toggle(void);
  uint32_t TickGet(void);
  uint32_t TickusGet(void);  
  void DelayMs(uint16_t ms);
  void Delayus(uint16_t ms);  

  void IIC_Initialize(uint8_t index);
  void IIC_StopTransfer(uint8_t index);
  Bool_t IIC_StartTransfer(uint8_t index, Bool_t restart);
  Bool_t IIC_TransmitOneByte(uint8_t index, uint8_t data);
  Bool_t IIC_Write(uint8_t index, uint8_t address, uint8_t *theData, uint8_t total);
  Bool_t IIC_Read(uint8_t index, uint8_t address, uint8_t *theData, uint8_t total);
  Bool_t IIC_IsChipThere(uint8_t index, uint8_t address);
  void IIC_Config(uint8_t index, uint32_t PortReg, uint32_t i2cConfig, uint32_t i2cClock);

  Bool_t SwitchOscillator(oscillators_t oscType);

  uint16_t ADC_Read(uint8_t index);

  void    PersistentSettings_Read();
  uint16_t PersistentSettings_Write();
  Bool_t  EraseValidAppFlag(void);

#endif

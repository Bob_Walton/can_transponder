/** *******************************************************************
 @file          uP_Drv_PIC32MX795L.c
 @brief         Initialisation of system
 @details       PIC32MX795L related initiation procedures and generic drivers
                for chip specific hardware such as LEDs.

 @copyright     Copyright (C) 2014 PowerbyProxi Ltd\n
                Copyright (C) 2014 PowerbyProxi Inc\n
                All rights reserved

 *********************************************************************/
#define __uP_Drv_PIC32MX795L_C

//*******************************************************************
//Includes
//Global Layer
#include "Global.h"
#include "HardwareConfig.h"

//Application Layer

//Driver Layer
#include "uP_Drv_PIC32MX795L.h"

//*******************************************************************
//#Defines
// IIC Port
#define I2C_CLOCK_FREQ              100000

//*******************************************************************
//#Typedefs
typedef enum
{
  EXCEP_IRQ = 0,          // interrupt
  EXCEP_AdEL = 4,         // address error exception (load or ifetch)
  EXCEP_AdES,             // address error exception (store)
  EXCEP_IBE,              // bus error (ifetch)
  EXCEP_DBE,              // bus error (load/store)
  EXCEP_Sys,              // syscall
  EXCEP_Bp,               // breakpoint
  EXCEP_RI,               // reserved instruction
  EXCEP_CpU,              // coprocessor unusable
  EXCEP_Overflow,         // arithmetic overflow
  EXCEP_Trap,             // trap (possible divide by zero)
  EXCEP_IS1 = 16,         // implementation specfic 1
  EXCEP_CEU,              // CorExtend Unuseable
  EXCEP_C2E               // coprocessor 2
}exception_code;

typedef struct
{
  /** @brief PLib Access name for PIC32MX*/
  uint32_t portReg;
  uint32_t i2cConfig;
  uint32_t i2cClock;
} i2c_interface_t;

typedef struct
{
  /** @brief ADC channel access number for PIC32MX*/
  uint8_t ADCInNum;
} adc_interface_t;

//*******************************************************************
//Prototypes
void ADC_Initialize(void);

void SysSettingsDefault(void);
void PersistentSettings_Upgrade(void);

//*******************************************************************
//Variables
// Internal counter to store Ticks.  This variable is incremented in an ISR and
// therefore must be marked volatile to prevent the compiler optimizer from
// reordering code to use this value in the main context while interrupts are
// disabled.
static volatile uint32_t internalTicks = 0x00;
static volatile uint32_t timeTicks = 0x00;

unsigned int lowerAddress = 0;                                    // to identify the read/write pointer address location

/** @brief  I2C interface array */
i2c_interface_t i2c_interface[I2CCHANNELS];

/** @brief  ADC interface array */
adc_interface_t adc_interface[ADCCHANNELS];

/** @brief  oscillator settings (full or low power)*/
oscillators_t oscillatorType;

//*******************************************************************
//Functions
//*******************************************************************

/** ******************************************************************
  @brief        Initialisation of system
  @details      System related initiation procedure.

*********************************************************************/
uP_Returns_t uP_SystemInit(void)
{
  System_Status = W1R_IDLE_STATUS;

  #ifdef _SysUses_Pcs
    uint16_t  retVal;                                                 // init return value
  #endif

  // Initialise application specific hardware
  INTEnableSystemMultiVectoredInt();                                // enable multi-vectored interrupts
  SYSTEMConfigPerformance(GetSystemClock());                        // enable optimal performance
  mOSCSetPBDIV(OSC_PB_DIV_1);                                       // use 1:1 CPU Core:Peripheral clocks
  DDPCONbits.JTAGEN = 0;                                            // disable JTAG port so we get our I/O pins back

  oscillatorType = fullSpeed;

  LED_Port_Direction = 0;                                           // status LED Setup
  LEDAUX_Port_Direction = 0;                                        // aux status LED Setup
  LED_ChangeStatus(True);                                           // turn LED on
  MasterSlave_Port_Direction = 1;                                   // input port

  #ifdef _SysUses_Pcs
    //here we need to check if i
    //read in first byte as config version to check if we need to upgrade
    memcpy(&SysSettings, (void *)FLASH_ADDR_SETTINGS, 1);

    if (SysSettings.CfgVersion == CFGVERSION)
    { //correct config version, read values into RAM
      PersistentSettings_Read();
    }
    else if (SysSettings.CfgVersion == (CFGVERSION - 1))
    { //translate old stored config version into new version, then save
      PersistentSettings_Upgrade();
      retVal = PersistentSettings_Write();
      if (retVal)
      {
        return (EE_WriteFail);
      }
    }
    else
    { //unknown sysSettings or corrupted memory, load defaults
      SysSettingsDefault();
      retVal = PersistentSettings_Write();
      if (retVal)
      {
        return (EE_WriteFail);
      }
    }

  #else
    SysSettingsDefault();
  #endif

  TickInit();                                                       // init tick module

  #ifndef __DEBUG
    EnableWDT();                                                    // enable the hardware WDT.
  #endif

  LED_ChangeStatus(False);                                          // turn LED off

  return(uP_OK);                                                    // return all ok
}

/** ******************************************************************
  @brief        Initialises the Tick manager module.
  @details      Configures the Tick module and any necessary hardware resources.
               This function is called only one during lifetime of the application.

  @pre          none
 *********************************************************************/
void TickInit(void)
{
  if(SysSettings.InterfacesSettings.UseDMX)
  {
    //configure Timer 1 using internal clock, 1:256 prescale
    if (oscillatorType == lowPower)
    {
      OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_256, T1_TICK_LOWPWR_DMX);
    }
    else
    {
      OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_256, T1_TICK_DMX);
    }
  }
  else
  {
    //configure Timer 1 using internal clock, 1:256 prescale
    if (oscillatorType == lowPower)
    {
      OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_256, T1_TICK_LOWPWR);
    }
    else
    {
      OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_256, T1_TICK);
    }
  }

  //config timer 1 interrupt
  ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_6);   //highest priority
}

/** ******************************************************************
  @brief        Change the status of the LED
  @details      none

  @pre          none

  @param [in]   status    on or off
*********************************************************************/
void LED_ChangeStatus(Bit_t status)
{
  if (status == Set)
  {
    LED_Port = 1;          // change LED status
    LEDAUX_Port = 1;       // change LED status
  }
  else
  {
    LED_Port = 0;          // change LED status
    LEDAUX_Port = 0;       // change LED status
  }
}

/** ******************************************************************
  @brief        Toggle the LED
  @details      none

  @pre          none
 *********************************************************************/
void LED_Toggle(void)
{
  LED_Port = !LED_Port;             // change LED status
  LEDAUX_Port = !LEDAUX_Port;       // change LED status
}

/** ******************************************************************
  @brief        Obtains the current Tick value.
  @details      This function retrieves the current Tick value, allowing timing and
                measurement code to be written in a non-blocking fashion.  This function
                retrieves the least significant 32 bits of the internal tick counter,
                and is useful for measuring time increments ranging from a few
                microseconds to a few hours.  Use TickGetDiv256 or TickGetDiv64K for
                longer periods of time.

  @pre          none

  @return       Lower 32 bits of the current Tick value.
*********************************************************************/
uint32_t TickGet(void)
{
  uint32_t result;

  //disable timer 1
  mT1IntEnable(0);

  result = internalTicks;

  //enable timer 1
  mT1IntEnable(1);

  return result;
}


/** ******************************************************************
  @brief        Obtains the current Tick value.
  @details      This function retrieves the current Tick value in micro second, allowing timing and
                measurement code to be written in a non-blocking fashion.  This function
                retrieves the least significant 32 bits of the internal tick counter,
                and is useful for measuring time increments ranging from a few
                microseconds to a few hours.  Use TickGetDiv256 or TickGetDiv64K for
                longer periods of time.

  @pre          none

  @return       Lower 32 bits of the current Tick value in micro second.
*********************************************************************/
uint32_t TickusGet(void)
{
  uint32_t result;

  //disable timer 1
  mT1IntEnable(0);

  result = timeTicks;

  //enable timer 1
  mT1IntEnable(1);

  return result;
}
/** ******************************************************************
  @brief        Produces a delay in milliseconds.
  @details      This function creates a delay.

  @param [in]   miltSeconds    Delay to be applied in mS

  @pre          none
 *********************************************************************/
void DelayMs(uint16_t miltSeconds)
{
  uint32_t  timeout;                                                  // delay timeout

  timeout = TickGet();                                                // set timeout

  while((TickGet() - timeout) <= miltSeconds);                        // check for timeout
}

/** ******************************************************************
  @brief        Produces a delay in microseconds.
  @details      This function creates a delay.

  @param [in]   micSeconds    Delay to be applied in uS

  @pre          none
 *********************************************************************/
void Delayus(uint16_t micSeconds)
{
  uint32_t timeout;                                                  // delay timeout

  timeout = TickusGet();                                             // set timeout

  while((TickusGet() - timeout) <= micSeconds);                      // check for timeout
}

/** ******************************************************************
  @brief        C32 Exception Handler
  @details      none

  @pre          none

  @param [in]   cause    cause of exception
  @param [in]   status   status
 *********************************************************************/
void _general_exception_handler(unsigned cause, unsigned status)
{
  exception_code Code;

  Code = (cause & 0x0000007C) >> 2;

  do
  {
    Nop();
    Nop();
  }
  while(1);   //stop here

  (void)Code;
}

/** ******************************************************************
  @brief        ISR via a Vector Table for PIC32MX
  @details      none

  @pre          none
 *********************************************************************/
void __ISR(_TIMER_1_VECTOR, IPL6AUTO) _T1Interrupt(void)
{
  if(SysSettings.InterfacesSettings.UseDMX)
  {
    timeTicks++;      //10us
    // Increment internal high tick counter
    if (timeTicks % 100 == 0 )
    {
      internalTicks++;    //1ms
    }
  }
  else
  {
    internalTicks++;      //1ms
  }

  // clear the interrupt flag
  mT1ClearIntFlag();
}

/** ******************************************************************
  @brief        Configure I2C bus
  @details      This function configures the I2C bus

  @param[in]    index       corresponding I2C channel
*********************************************************************/
void IIC_Config(uint8_t index, uint32_t PortReg, uint32_t i2cConfig, uint32_t i2cClock)
{
  i2c_interface[index].portReg = PortReg;
  i2c_interface[index].i2cConfig = i2cConfig;
  i2c_interface[index].i2cClock = i2cClock;
}

/** ******************************************************************
  @brief        Initialize Master I2C bus
  @details      This function configures and enables the I2C

  @param[in]    index       corresponding I2C channel
*********************************************************************/
void IIC_Initialize(uint8_t index)
{
  //disables module to reset status register
//  I2CEnable(i2c_interface[index].portReg, False);
//
//  I2C1_SDA = 1;                                             //set SDA output as starting high to prevent any false start/stop condition detections
//  I2C1_SDA_TRIS = 0;                                        //set direction
//
//  // IIC Setup
//  I2CConfigure(i2c_interface[index].portReg, i2c_interface[index].i2cConfig);
//  if(oscillatorType == lowPower)
//  {
//    I2CSetFrequency(i2c_interface[index].portReg, CLOCK_LOWPOWER, i2c_interface[index].i2cClock);
//  }
//  else
//  {
//    I2CSetFrequency(i2c_interface[index].portReg, GetPeripheralClock(), i2c_interface[index].i2cClock);
//  }
//  I2CEnable(i2c_interface[index].portReg, True);
}

/** ******************************************************************
  @brief        Starts (or restarts) a transfer to/from the EEPROM.
  @details      This routine starts (or restarts) a transfer to/from
                the EEPROM, waiting (in a blocking loop) until the
                start (or re-start) condition has completed.

  @pre          The I2C module must have been initialized.

  @param[in]    index       corresponding I2C channel
  @param[in]    restart     start or restart

  @return       True if successful
*********************************************************************/
Bool_t IIC_StartTransfer(uint8_t index, Bool_t restart)
{
  // Send the Start (or restart) signal
  if(restart)
  {
    if(I2CRepeatStart(i2c_interface[index].portReg) == I2C_MASTER_BUS_COLLISION)
    {
      return False;
    }
  }
  else
  {
    // Wait for the bus to be idle, then start the transfer
    while (!I2CBusIsIdle(i2c_interface[index].portReg));

    if(I2CStart(i2c_interface[index].portReg) != I2C_SUCCESS)
    {
      return False;
    }
  }

  while(!(I2CGetStatus(i2c_interface[index].portReg) & I2C_START));  // Wait for the signal to complete

  return True;
}

/** ******************************************************************
  @brief        Stops a transfer to/from the EEPROM.
  @details      This routine stops a transfer to/from
                the EEPROM, waiting (in a blocking loop) until the
                stop condition has completed.

  @pre          The I2C module must have been initialized.

  @param[in]    index       corresponding I2C channel
*********************************************************************/
void IIC_StopTransfer(uint8_t index)
{
  // Send the Stop signal
  I2CStop(i2c_interface[index].portReg);

  while(!(I2CGetStatus(i2c_interface[index].portReg) & I2C_STOP));  // Wait for the signal to complete
  return;
}

/** ******************************************************************
  @brief        Stops a transfer to/from the EEPROM (for read operations only as plib API doesn't work properly)
  @details      This routine stops a transfer to/from
                the EEPROM, waiting (in a blocking loop) until the
                stop condition has completed.

  @pre          The I2C module must have been initialized.

  @param[in]    index       corresponding I2C channel
*********************************************************************/
void IIC_ReadStopTransfer(uint8_t index)
{
  // Send the Stop signal
  I2C2CONbits.PEN = 1;

  while(!(I2CGetStatus(i2c_interface[index].portReg) & I2C_STOP))   // Wait for the signal to complete
  {
    I2C2CONbits.PEN = 1;
  }
}

/** ******************************************************************
  @brief        This transmits one byte on the I2C bus.
  @details      This transmits one byte on the I2C bus, and reports
                errors for any bus collisions.

  @pre          The I2C module must have been initialized.

  @param[in]    index       corresponding I2C channel
  @param[in]    data        Data byte to transmit

  @return       True if sent successfully
*********************************************************************/
Bool_t IIC_TransmitOneByte(uint8_t index, uint8_t data)
{
  while (!I2CTransmitterIsReady(i2c_interface[index].portReg));

  // Transmit the byte
  if(I2CSendByte(i2c_interface[index].portReg, data) == I2C_MASTER_BUS_COLLISION)
  {
    return False;
  }

  // Wait for the transmission to finish
  while (!I2CTransmissionHasCompleted(i2c_interface[index].portReg));

  // Check that the reciever has ACK'd the bus
  if(!I2CByteWasAcknowledged(i2c_interface[index].portReg))
  {
    return False;
  }

  return True;
}

/** ******************************************************************
  @brief        checks if Chip responds
  @details      This routine checks if there is a response on I2C
                when sending one byte

  @pre          The I2C module must have been initialized.

  @param[in]    index       corresponding I2C channel
  @param[in]    address     dummy byte

  @return       True if chip available
*********************************************************************/
Bool_t IIC_IsChipThere(uint8_t index, uint8_t address)
{
  Bool_t result = True;
  uint32_t status;

  if(IIC_StartTransfer(index, False))
  {
    // Send Address
    result = IIC_TransmitOneByte(index, address);
    status = I2CGetStatus(i2c_interface[index].portReg);
    if (status & (I2C_BYTE_ACKNOWLEDGED | I2C_ARBITRATION_LOSS))
    {
      result = False;
    }
  }

  // End the transfer
  IIC_StopTransfer(index);

  return result;
}

/** ******************************************************************
  @brief        Writes data to the I2C
  @details      This routine writes data over I2C for a given address.

  @pre          The I2C module must have been initialized.

  @param[in]    index       corresponding I2C channel
  @param[in]    address     address of data
  @param[in]    *theData    data to be sent
  @param[in]    total       amount of bytes

  @return       True if write successful
*********************************************************************/
Bool_t IIC_Write(uint8_t index, uint8_t address, uint8_t *theData, uint8_t total)
{
  Bool_t result = False;

  // Validate Address
  address &= 0xFE;

  if(IIC_StartTransfer(index, False))
  {
    // Send Address
    result = IIC_TransmitOneByte(index, address);
    result &= I2CByteWasAcknowledged(i2c_interface[index].portReg);

    // Send Data if the chip Ack'ed
    while (result && (total != 0))
    {
      // Transmit a byte
      if(IIC_TransmitOneByte(index, *theData))
      {
        // Advance to the next byte
        total--;
      }
      else
      {
        result = False;
      }

      theData++;

      // Verify that the byte was acknowledged
      if(!I2CByteWasAcknowledged(i2c_interface[index].portReg))
      {
        result = False;
      }
    }
  
  }

  // End the transfer
  IIC_StopTransfer(index);

  return result;
}

/** ******************************************************************
 @brief        Reads data from the I2C
 @details      This routine reads data from the I2C for a given address.

 @pre          The I2C module must have been initialized.

 @param[in]    index       corresponding I2C channel
 @param[in]    address     address of data
 @param[in]    *theData    data to receive
 @param[in]    total       amount of bytes

 @return       True if write successful
 *********************************************************************/
Bool_t IIC_Read(uint8_t index, uint8_t address, uint8_t *theData, uint8_t total)
{
  // Change address to read type (should be 0x39)
  address |= I2C_READ;

  // Send a Repeated Started condition
  if(!IIC_StartTransfer(index, True))
  {
    return False;
  }

  // Transmit the address with the READ bit set
  if(!IIC_TransmitOneByte(index, address))
  {
    return False;
  }

  while(total)
  {
    //enable receiver (have to do this every time.
    if(I2CReceiverEnable(i2c_interface[index].portReg, True) == I2C_RECEIVE_OVERFLOW)
    {
      return False;
    }

    //wait for new data
    while(!I2CReceivedDataIsAvailable(i2c_interface[index].portReg));

    //prepare out ACK (want more bytes) or NACK (the last byte). I2CAcknowledgeByte function doesn't actually send out anything straight away
    (total > 1) ? (I2CAcknowledgeByte(i2c_interface[index].portReg, True)) : (I2CAcknowledgeByte(i2c_interface[index].portReg, False));   //NAK on last byte
    //wait for (N)ACK to actually be sent
    while(!I2CAcknowledgeHasCompleted(i2c_interface[index].portReg));

    *theData = I2CGetByte(i2c_interface[index].portReg);
    theData++;
    total--;
  }

  // Disable receiver
  if(I2CReceiverEnable(i2c_interface[index].portReg, False) == I2C_RECEIVE_OVERFLOW)
  {
    return False;
  }

  // End the transfer with special function (only works with I2C2 module)
  IIC_ReadStopTransfer(index);

  return True;
}

/** ******************************************************************
  @brief        Initialize ADC
  @details      This function configures and enables the ADC
*********************************************************************/
void ADC_Initialize(void)
{
  adc_interface[0].ADCInNum = ADC_Channel1;   //setup channel structure

  CloseADC10();

  SetChanADC10(ADC_Channel_Config);

  OpenADC10(ADC_Config1, ADC_Config2, ADC_Config3, ADC_Port, ADC_Configscan);

  EnableADC10();
}

/** ******************************************************************
  @brief        Read ADC
  @details      This function reads one ADC channel

  @param[in]    index       corresponding ADC channel

  @return       ADC value
*********************************************************************/
uint16_t ADC_Read(uint8_t index)
{
  return ReadADC10(adc_interface[index].ADCInNum);
}

/** ******************************************************************
 @brief        switches oscillator between full speed (72mhz) and low speed (4mhz)
 @details      Switches to internal RC oscillator (8mhz, with 2x divider for 4mhz)
               for use in low power mode (saves around 20mA). tempoararily disables
               interrupts and DMA, sets periphial clock diviider to
               Tick init and I2C init should be called after changing oscillators agian
               to ensure they are set up correctly
   
 @param[in]    oscType    ofscillator type selected with appropriate enum

 @return       true on success, false on fail
 *********************************************************************/
Bool_t SwitchOscillator(oscillators_t oscType)
{
  //NOTE: this function required the change of some configuration bits to enable clock switching and disable clock failure monitoring
  //check that we have a valid oscillator type
  if((oscType != lowPower) && (oscType != fullSpeed))
  {
    return False;
  }

  //disable interrupts, DMA
  INTDisableInterrupts();
  DmaEnable(False);

  //write unlock sequence
  SYSKEY = 0x0;
  SYSKEY = 0xAA996655;
  SYSKEY = 0x556699AA;

  //set new oscillator source, set flag
  if(oscType == lowPower)
  {
    OSCCONbits.NOSC = 0b111;   //'fast' RC oscillator (divided by FRCDIV bits)
    OSCCONbits.FRCDIV = 0b001;   //2x divider for 4 meg clock
    OSCCONbits.PBDIV = 00;      //no periphial devider
    oscillatorType = lowPower;
  }
  else if(oscType == fullSpeed)
  {
    OSCCONbits.NOSC = 0b011;
    oscillatorType = fullSpeed;
  }
  else
  {
    return False;
  }

  //set oscillator switch bit
  OSCCONbits.OSWEN = True;

  //clear unlock sequence
  SYSKEY = 0x0;

  //block until switch successful
  while(OSCCONbits.OSWEN == True);

  //re-enable interrupts and DMA
  INTEnableInterrupts();
  DmaEnable(True);

  return True;   // on success
}

/** ******************************************************************
  @brief        System settings default
  @details      This function loads defaults from non-volatile memory
                for saving to flash.
*********************************************************************/
void SysSettingsDefault(void)
{
  uint16_t Index;
  Bool_t gpiInverts[] = GPIO_DEFAULT_IN_INVERTS, gpoInverts[] = GPIO_DEFAULT_OUT_INVERTS;

  SysSettings.AddressSettings.IsMaster = MasterSlave_Port;           // set Master/Slave

  SysSettings.CfgVersion  = CFGVERSION;

  //Interface defaults
  #ifndef _SysUses_Pcs
    #if defined _SysUses_CAN
      SysSettings.InterfacesSettings.CAN_Enable = True;
    #else
      SysSettings.InterfacesSettings.CAN_Enable = False;
    #endif

    #if defined _SysUses_Ethernet
      SysSettings.InterfacesSettings.Ethernet_Enable = True;
    #else
      SysSettings.InterfacesSettings.Ethernet_Enable = False;
    #endif

    #if defined _SysUses_GPIOs
      SysSettings.InterfacesSettings.GPIO_Enable = True;
    #else
      SysSettings.InterfacesSettings.GPIO_Enable = False;
    #endif

    #if defined _SysUses_UARTs
      SysSettings.InterfacesSettings.RS232_Enable = True;
      SysSettings.InterfacesSettings.RS485_Enable = True;
      (UART_DEFAULT_USE_RS422)?(SysSettings.InterfacesSettings.RS422_Enable = True):(SysSettings.InterfacesSettings.RS422_Enable = False);
    #else
      SysSettings.InterfacesSettings.RS232_Enable = False;
      SysSettings.InterfacesSettings.RS485_Enable = False;
      SysSettings.InterfacesSettings.RS422_Enable = False;
    #endif

    (UART_DEFAULT_USE_DMX)?(SysSettings.InterfacesSettings.UseDMX = True):(SysSettings.InterfacesSettings.UseDMX = False);

    #if defined _SysUses_4to20mA
      SysSettings.InterfacesSettings.mA4to20_Enable = True;
    #else
      SysSettings.InterfacesSettings.mA4to20_Enable = False;
    #endif

    #if defined _SysUses_0to10V
      SysSettings.InterfacesSettings.v0to10_Enable = True;
    #else
      SysSettings.InterfacesSettings.v0to10_Enable = False;
    #endif

    #ifdef _SysUses_UI
      SysSettings.InterfacesSettings.UI_Enable = True;
    #else
      SysSettings.InterfacesSettings.UI_Enable = False;
    #endif
  #else
    SysSettings.InterfacesSettings.RS232_Enable     = False;
    SysSettings.InterfacesSettings.RS485_Enable     = False;
    SysSettings.InterfacesSettings.RS422_Enable     = False;
    SysSettings.InterfacesSettings.CAN_Enable       = False;
    SysSettings.InterfacesSettings.GPIO_Enable      = False;
    SysSettings.InterfacesSettings.Ethernet_Enable  = False;
    SysSettings.InterfacesSettings.mA4to20_Enable   = False;
    SysSettings.InterfacesSettings.v0to10_Enable    = False;
    SysSettings.InterfacesSettings.UseDMX           = False;
  #endif

  SysSettings.InterfacesSettings.RS232_ChannelCount = UART_DEFAULT_RS232_CHANNELS;
  SysSettings.InterfacesSettings.RS485_ChannelCount = UART_DEFAULT_RS485_CHANNELS;
  SysSettings.InterfacesSettings.CAN_ChannelCount = CAN_DEFAULT_CHANNELS;
  SysSettings.InterfacesSettings.Eth_ChannelCount = ETHERNET_DEFAULT_CHANNELS;
  SysSettings.InterfacesSettings.GPIO_ChannelCount_IN = GPIO_DEFAULT_CHANNELS_IN;
  SysSettings.InterfacesSettings.GPIO_ChannelCount_OUT = GPIO_DEFAULT_CHANNELS_OUT;
  SysSettings.InterfacesSettings.mA4to20_ChannelCount_IN = S4to20_DEFAULT_CHANNELS_IN;
  SysSettings.InterfacesSettings.mA4to20_ChannelCount_OUT = S4to20_DEFAULT_CHANNELS_OUT;
  SysSettings.InterfacesSettings.v0to10_ChannelCount_IN = S0to10_DEFAULT_CHANNELS_IN;
  SysSettings.InterfacesSettings.v0to10_ChannelCount_OUT = S0to10_DEFAULT_CHANNELS_OUT;

  //Project defaults
  //cleared to 0
  memset(SysSettings.ProjectSettings.ProjectName, 0, sizeof(SysSettings.ProjectSettings.ProjectName));
  memset(SysSettings.ProjectSettings.ShortDescription, 0, sizeof(SysSettings.ProjectSettings.ShortDescription));
  memset(SysSettings.ProjectSettings.BuildDate, 0, sizeof(SysSettings.ProjectSettings.BuildDate));
  memset(SysSettings.ProjectSettings.SoftwareVersion, 0, sizeof(SysSettings.ProjectSettings.SoftwareVersion));
  memset(SysSettings.ProjectSettings.SerialNumber, 0, sizeof(SysSettings.ProjectSettings.SerialNumber));

  //Rf defaults
  SysSettings.RF_Settings.RFFlushTime = RF_DEFAULT_FLUSHTIME;                  // flushtime
  SysSettings.RF_Settings.RFMaxPayload= RF_DEFAULT_MAXPAYLOAD;                 // max payload
  SysSettings.RF_Settings.Spoofing    = RF_DEFAULT_SPOOFING;                   // 1 = spoofing on, 0 = spoofing off
  SysSettings.RF_Settings.SpoofTimer  = RF_DEFAULT_SPOOFTIMER;                 // spoof timer in ms
  SysSettings.RF_Settings.SpoofMaxNo  = RF_DEFAULT_SPOOFMAX;                   // max number of spoofs
  SysSettings.RF_Settings.RadioChannel= RF_DEFAULT_CHANNEL;                    // max default

  //Address defaults
  #ifdef RF_nRF51822
    // setup IP addresses
    SysSettings.AddressSettings.MyMACAddr[0] = Tx_Address;
    SysSettings.AddressSettings.DestMACAddr[0] = Rx_Address;
    #else //Redpine
    // setup MAC addresses
    #if  RedpineMacAddress
      SysSettings.AddressSettings.MyMACAddr[0] = Redpine_MAC_Byte1;
      SysSettings.AddressSettings.MyMACAddr[1] = Redpine_MAC_Byte2;
      SysSettings.AddressSettings.MyMACAddr[2] = Redpine_MAC_Byte3;
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[3] = Addr_MasterMacB2):(SysSettings.AddressSettings.MyMACAddr[3] = Addr_SlaveMacB2);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[4] = Addr_MasterMacB3):(SysSettings.AddressSettings.MyMACAddr[4] = Addr_SlaveMacB3);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[5] = Addr_MasterMacB4):(SysSettings.AddressSettings.MyMACAddr[5] = Addr_SlaveMacB4);

      SysSettings.AddressSettings.DestMACAddr[0] = Redpine_MAC_Byte1;
      SysSettings.AddressSettings.DestMACAddr[1] = Redpine_MAC_Byte2;
      SysSettings.AddressSettings.DestMACAddr[2] = Redpine_MAC_Byte3;
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[3] = Addr_SlaveMacB2):(SysSettings.AddressSettings.DestMACAddr[3] = Addr_MasterMacB2);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[4] = Addr_SlaveMacB3):(SysSettings.AddressSettings.DestMACAddr[4] = Addr_MasterMacB3);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[5] = Addr_SlaveMacB4):(SysSettings.AddressSettings.DestMACAddr[5] = Addr_MasterMacB4);
    #else
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[0] = Default_MAC_Byte1):(SysSettings.AddressSettings.MyMACAddr[0] = Default_MAC_Byte1);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[1] = Default_MAC_Byte2):(SysSettings.AddressSettings.MyMACAddr[1] = Default_MAC_Byte2);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[2] = Addr_MasterMacB1):(SysSettings.AddressSettings.MyMACAddr[2] = Addr_SlaveMacB1);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[3] = Addr_MasterMacB2):(SysSettings.AddressSettings.MyMACAddr[3] = Addr_SlaveMacB2);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[4] = Addr_MasterMacB3):(SysSettings.AddressSettings.MyMACAddr[4] = Addr_SlaveMacB3);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.MyMACAddr[5] = Addr_MasterMacB4):(SysSettings.AddressSettings.MyMACAddr[5] = Addr_SlaveMacB4);

      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[0] = Default_MAC_Byte1):(SysSettings.AddressSettings.DestMACAddr[0] = Default_MAC_Byte1);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[1] = Default_MAC_Byte2):(SysSettings.AddressSettings.DestMACAddr[1] = Default_MAC_Byte2);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[2] = Addr_SlaveMacB1):(SysSettings.AddressSettings.DestMACAddr[2] = Addr_MasterMacB1);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[3] = Addr_SlaveMacB2):(SysSettings.AddressSettings.DestMACAddr[3] = Addr_MasterMacB2);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[4] = Addr_SlaveMacB3):(SysSettings.AddressSettings.DestMACAddr[4] = Addr_MasterMacB3);
      (SysSettings.AddressSettings.IsMaster)?(SysSettings.AddressSettings.DestMACAddr[5] = Addr_SlaveMacB4):(SysSettings.AddressSettings.DestMACAddr[5] = Addr_MasterMacB4);
    #endif
  #endif

  memset(SysSettings.AddressSettings.SSID, 0, sizeof(SysSettings.AddressSettings.SSID));
  memcpy(SysSettings.AddressSettings.SSID, Rf_SSID, sizeof(Rf_SSID) - 1);

  //Status defaults
  SysSettings.RF_Settings.StatusPeriod = UPDATEPERIOD;                     // default update period in mS

  // UI defaults
  SysSettings.UI_Settings.IsRFIDset = UI_DEFAULT_IS_PAIRED;
  SysSettings.UI_Settings.PairID = UI_DEFAULT_PAIRED_ID;

  //UART defaults channel 1
  SysSettings.UART_Setups[0].BaudRate = UART_DEFAULT_BAUDRATE;             // 2400, 4800, 9600, 19200, 38400, 57600, 115200
  SysSettings.UART_Setups[0].DataBits = UART_DEFAULT_BITS;                 // 7, 8
  SysSettings.UART_Setups[0].Parity   = UART_DEFAULT_PARITY;               // 1 = Even, 2 = Odd, 3 = None
  SysSettings.UART_Setups[0].StopBits = UART_DEFAULT_STOPS;                // 1, 2
  //UART defaults channel 2
  SysSettings.UART_Setups[1].BaudRate = UART_DEFAULT_BAUDRATE;             // 2400, 4800, 9600, 19200, 38400, 57600, 115200
  SysSettings.UART_Setups[1].DataBits = UART_DEFAULT_BITS;                 // 7, 8
  SysSettings.UART_Setups[1].Parity   = UART_DEFAULT_PARITY;               // 1 = Even, 2 = Odd, 3 = None
  SysSettings.UART_Setups[1].StopBits = UART_DEFAULT_STOPS;                // 1, 2
  //UART defaults channel 3
  SysSettings.UART_Setups[2].BaudRate = UART_DEFAULT_BAUDRATE;             // 2400, 4800, 9600, 19200, 38400, 57600, 115200
  SysSettings.UART_Setups[2].DataBits = UART_DEFAULT_BITS;                 // 7, 8
  SysSettings.UART_Setups[2].Parity   = UART_DEFAULT_PARITY;               // 1 = Even, 2 = Odd, 3 = None
  SysSettings.UART_Setups[2].StopBits = UART_DEFAULT_STOPS;                // 1, 2
  //UART defaults channel 4
  SysSettings.UART_Setups[3].BaudRate = UART_DEFAULT_BAUDRATE;             // 2400, 4800, 9600, 19200, 38400, 57600, 115200
  SysSettings.UART_Setups[3].DataBits = UART_DEFAULT_BITS;                 // 7, 8
  SysSettings.UART_Setups[3].Parity   = UART_DEFAULT_PARITY;               // 1 = Even, 2 = Odd, 3 = None
  SysSettings.UART_Setups[3].StopBits = UART_DEFAULT_STOPS;                // 1, 2

  //CAN defaults
  for(Index = 0; Index < CAN_CHANNELS_MAX; Index++)
  {
    SysSettings.CAN_Setups[Index].BaudRate = CAN_DEFAULT_BAUDRATE;         // default baud rate 1000000 (50000, 250000, 500000, 1000000)
    SysSettings.CAN_Setups[Index].AutoBaud = CAN_DEFAULT_AUTO_BAUD;        // 1 = auto baud on, 0 = auto baud off
  }

  //Ethernet defaults
  for(Index = 0; Index < ETHERNET_CHANNELS_MAX; Index++)
  {
    SysSettings.EthernetSetup[Index].Mode = ETHERNET_DEFAULT_TYPE;         // default Ethernet setup
  }

  //GPIO defaults
  for(Index = 0; Index < GPIO_CHANNELS_MAX; Index++ )
  {
    SysSettings.GPIOSetup[Index].In_Invert = gpiInverts[Index];
    SysSettings.GPIOSetup[Index].Out_Invert = gpoInverts[Index];
  }

  //4-20mA defaults
  for(Index = 0; Index < S4to20CHANNELS_MAX; Index++ )
  {
    SysSettings.mA4to20Setup[Index].Input_M = INPUT_4to20mA_M;                 // M of Y=Mx+C
    SysSettings.mA4to20Setup[Index].Input_C = INPUT_4to20mA_C;                 // C of Y=Mx+C
    SysSettings.mA4to20Setup[Index].Output_M = OUTPUT_4to20mA_M;               // M of Y=Mx+C
    SysSettings.mA4to20Setup[Index].Output_C = OUTPUT_4to20mA_C;               // C of Y=Mx+C
  }

  //0-10V defaults
  for(Index = 0; Index < S0to10CHANNELS_MAX; Index++ )
  {
    SysSettings.v0to10Setup[Index].Input_M = INPUT_0to10V_M;                    // M of Y=Mx+C
    SysSettings.v0to10Setup[Index].Input_C = INPUT_0to10V_C;                    // C of Y=Mx+C
    SysSettings.v0to10Setup[Index].Output_M = OUTPUT_0to10V_M;                  // M of Y=Mx+C
    SysSettings.v0to10Setup[Index].Output_C = OUTPUT_0to10V_C;                  // C of Y=Mx+C
  }
}

/** ******************************************************************
 @brief        copies memory from flash to SysSettings structure
 @details      Copies the SysSettings structure from the location fixed
               in the header file to the address of the SysSetting structure

 @pre          SysSettings structure defined.
 *********************************************************************/
void PersistentSettings_Read(void)
{
  memcpy(&SysSettings, (void const *)FLASH_ADDR_SETTINGS, sizeof(SysSettings));
  SysSettings.AddressSettings.IsMaster = MasterSlave_Port;          // set Master/Slave
}

/** ******************************************************************
 @brief        copies memory from flash to SysSettings structure
 @details      Copies the SysSettings structure from the location fixed
               in the header file to the address of the SysSetting structure.

 @pre          SysSettings structure defined.

 @returns      returns value based on success of flash operation. 0 for success
 *********************************************************************/
uint16_t PersistentSettings_Write()
{
  uint16_t i;
  uint16_t numBytes;
  uint8_t status = 0;
  uint8_t *originAddr = (uint8_t *)&SysSettings;

  uint32_t tempWord;

  numBytes = sizeof(SysSettings);

  NVMErasePage((void*)FLASH_ADDR_SETTINGS);                                    //erases 2 pages of sysSetttings
  NVMErasePage((void*)(FLASH_ADDR_SETTINGS + FLASH_PAGE_SIZE));

  for(i = 0; i < numBytes; i += 4)
  {
    uint8_t bytesToCopy;

    tempWord = 0x00;   //clear temp data
    bytesToCopy = ((numBytes - i) < 4) ? (numBytes - i) : (4);   //gets min of 4 or remaining bytes to copy;

    memcpy(&tempWord, originAddr + i, bytesToCopy);
    status = NVMWriteWord((void*)(FLASH_ADDR_SETTINGS + i), tempWord);
    if(status > FLASH_SUCCESS)
    {
      return (status);
    }
  }
  return (FLASH_SUCCESS);
}

/** ******************************************************************
 @brief        translates old sysSettings structure into new structure
 @details      Reads old structure stored in flash into temp struct
               Copies relevant data into new sysSettings structure (in RAM)

 @returns      nada
 *********************************************************************/
void PersistentSettings_Upgrade(void)
{
  
}

/** ******************************************************************
 @brief        Erases the valid application flag, so on next restart will enter bootloader
 @details      Clear is based on fixed address at end of memory. Set to all 0s (low)
                so erase not needed

 @returns      returns value based on success of flash operation. 0 for success
 *********************************************************************/
Bool_t EraseValidAppFlag(void)
{
  uint16_t result = 0;

  result = NVMWriteWord((void*)FLASH_ADDR_CRC, 0x00000000);

  if(result > FLASH_SUCCESS)
  {
    return False;
  }
  return True;
}
